from bs4 import BeautifulSoup
from urllib.request import Request, urlopen
from discord.ext import commands
from os.path import expanduser
import json, discord, time, functions, codecs, random
embedColour = 0x50bdfe
class misc(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.harn = '202745416062599168'
    
    @commands.command()
    async def tikken(self,ctx,user):
        """Tikker <user>"""
        print(user[3:-1])
        if (str(ctx.message.author.id) == self.harn):
            with open(expanduser("~/credentials/spillere.json"), 'r',encoding='utf8') as f:
                data = json.load(f)
            if (user[3:-1] in data['spillere']):
                self.harn = str(user[3:-1])
                await ctx.message.add_reaction("✔️")
            else:
                await ctx.message.add_reaction("❌")
        else:
            await ctx.message.add_reaction("❗")
    
    @commands.command()
    async def blimed(self,ctx):
        """Blir med på leken"""
        with open(expanduser("~/credentials/spillere.json"), 'r',encoding='utf8') as f:
            data = json.load(f)
        if (str(ctx.message.author.id) not in data['spillere']):
            data['spillere'].append(str(ctx.message.author.id))
            with open(expanduser("~/credentials/spillere.json"), 'w',encoding='utf8') as f:
                json.dump(data,f)
            
    @commands.command()
    async def harn(self,ctx):
        """Viser hvem som harn"""
        embed = discord.Embed(description=f"<@{self.harn}> harn", color=0x50bdfe)
        await ctx.send(embed=embed)
        
    @commands.command()
    async def med(self,ctx):
        """Viser hevem som er med"""
        liste = []
        with open(expanduser("~/credentials/spillere.json"), 'r',encoding='utf8') as f:
            data = json.load(f)
        for user in data['spillere']:
            liste.append(f"<@!{user}>")
        embed = discord.Embed(description=f"{liste} er med", color=0x50bdfe)
        await ctx.send(embed=embed)

    @commands.command()
    async def olje(self,ctx):
        """Skriver verdien av oljefondet"""
        functions.log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"command")
        try:
            req = Request('https://www.nbim.no/LiveNavHandler/Current.ashx', headers={'User-Agent': 'Mozilla/5.0'})
        except Exception as e:
            await ctx.send("Når ikke oljefondet.")
        html = urlopen(req).read()
        soup = BeautifulSoup(html, "html.parser")
        stir = str(soup)
        oil = json.loads(stir)
        liveNavList = oil["d"]["liveNavList"][0]
        start = liveNavList["startSecond"]
        oilNumber = liveNavList["values"][start]["Value"]
        print("Oljefondet er verd " + oilNumber)
        await ctx.send(oilNumber + " NOK")

    @commands.command()
    async def nytt_ar(self,ctx):
        """Tid til nyttar"""
        functions.log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"command")
        nyttar = 1640991599#1609455600
        sLeft = (nyttar - time.time())
        mLeft = int(sLeft/60)%60
        hLeft = int(sLeft/(60*60))
        sLeft = int(sLeft%60)
        message = f"{hLeft} timer {mLeft} minutter {sLeft} sekunder igjen! 🎆"
        await ctx.send(message)

    @commands.command()
    async def slap(self,ctx,user):
        """Slapper <brukeren>"""
        with open(expanduser("~/credentials/liste.json"), 'r',encoding='utf8') as f:
            data = json.load(f)
        gave=random.choice(data["gave"])
        if str(ctx.message.channel.id) != '0':#'298411656084717569':
            await ctx.send("Slapper " + user + " med " + gave)
        else:
            await ctx.message.add_reaction('❌')

    @commands.command()
    async def tellerstatus(self,ctx):
        """Status pa tellinga"""
        with open(expanduser("~/credentials/teller.json"), 'r',encoding='utf8') as f:
            data = json.load(f)
    #    await ctx.send(str(data["leder"]) + " leder med " + str(data["ledertall"]))
        desc="Current: " + str(data["current"])
        embed = discord.Embed(title="Tellestatus", description=desc, color=embedColour)
        for user in data["users"]:
            userName = data["index"][user]
            embed.add_field(name=userName, value=str(data["users"][user]), inline=True)
        await ctx.send(embed=embed)

def setup(bot):
    n = misc(bot)
    bot.add_cog(n)
