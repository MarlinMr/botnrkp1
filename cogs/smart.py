from discord.ext import commands
import json
import random
import codecs
from os.path import expanduser

class smart(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def klapp(self,ctx):
        """Utfører SMART handlinger {Verb} {preposisjon} {objekt} til {subjekt}"""
        with open(expanduser("~/credentials/smart.json"), 'r',encoding='utf8') as f:
            data = json.load(f)
            verb=random.choice(data["verbs"])
            preposition=random.choice(data["prepositions"])
            object=random.choice(data["objects"])
            subject=random.choice(data["subjects"])
        await ctx.send(":clap: "+verb+" "+preposition+" "+object+" til <@"+subject+">!")

    @commands.command()
    async def addMe(self,ctx):
        """Legger deg til i lista over subjekter"""
        with open(expanduser("~/credentials/smart.json"), 'r',encoding='utf8') as f:
            data = json.load(f)
            if str(ctx.message.author.id) in data["subjects"]:
                await ctx.send("Du er alt på lista")
                return 0
            data["subjects"].append(str(ctx.message.author.id))
        with open(expanduser("~/credentials/smart.json"), 'w',encoding='utf8') as f:
            json.dump(data,f)
        await ctx.send("La deg til på lista")

    @commands.command()
    async def addObject(self,ctx,object):
        """Legger til et object i lista av objecter"""
        with open(expanduser("~/credentials/smart.json"), 'r',encoding='utf8') as f:
            data = json.load(f)
            if str(object) in data["objects"]:
                await ctx.send("Den er alt på lista")
                return 0
            elif "@" in str(object):
                await ctx.send("<:CarlsenPalm:330483537298063360>")
                return 0
            data["objects"].append(str(object))
        with open(expanduser("~/credentials/smart.json"), 'w',encoding='utf8') as f:
            json.dump(data,f)
        await ctx.send("La den til på lista")
        
    @commands.command()
    async def addVerb(self,ctx,verb):
        """Legger til et verb i lista av verb"""
        with open(expanduser("~/credentials/smart.json"), 'r',encoding='utf8') as f:
            data = json.load(f)
            if str(verb) in data["verbs"]:
                await ctx.send("Den er alt på lista")
                return 0
            elif "@" in str(verb):
                await ctx.send("<:CarlsenPalm:330483537298063360>")
                return 0
            data["verbs"].append(str(verb))
        with open(expanduser("~/credentials/smart.json"), 'w',encoding='utf8') as f:
            json.dump(data,f)
        await ctx.send("La den til på lista")

    @commands.command()
    async def addPreposition(self,ctx,preposition):
        """Legger til et preposition i lista av prepositions"""
        with open(expanduser("~/credentials/smart.json"), 'r',encoding='utf8') as f:
            data = json.load(f)
            if str(preposition) in data["prepositions"]:
                await ctx.send("Den er alt på lista")
                return 0
            elif "@" in str(preposition):
                await ctx.send("<:CarlsenPalm:330483537298063360>")
                return 0
            data["prepositions"].append(str(preposition))
        with open(expanduser("~/credentials/smart.json"), 'w',encoding='utf8') as f:
            json.dump(data,f)
        await ctx.send("La den til på lista")

def setup(bot):
    n = smart(bot)
    bot.add_cog(n)
