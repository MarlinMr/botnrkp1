from discord.ext import commands
import os
import discord
import json
import codecs
import functions
from os.path import expanduser
import yfinance as yf
embedColour = 0x50bdfe
status_file = expanduser("~/.config/invest/invest.json")

def get_investors():
    with open(status_file, "r", encoding='utf8') as f:
        data = json.load(f)
    return data

class invest(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def investors(self,ctx):
        """Lister alle investors og deres score"""
        data = get_investors()
        embed=discord.Embed(title="Investors", color=embedColour)
        for user in data["investors"]:
            try:
                embed.add_field(name="<@" + user + ">", value=data["investors"][user]["ek"])
                print(user)
                print(data["investors"][user]["ek"])
            except Exception as E:
                pass
        await ctx.send(embed=embed)


    @commands.command()
    async def openaccount(self,ctx):
        """Starter din investeringskarriere!"""
        data = get_investors()
        if str(ctx.message.author.id) in data["investors"]:
            await ctx.message.add_reaction("❌")
        else:
            holdings = {"balance":"100000", "portfolio":{}}
            data["investors"][ctx.message.author.id] = holdings
            with open(status_file, "w", encoding='utf8') as f:
                json.dump(data,f)
            await ctx.message.add_reaction("✅")

    @commands.command()
    async def myaccount(self,ctx):
        """Viser din account"""
        data = get_investors()
        if str(ctx.message.author.id) not in data["investors"]:
            await ctx.message.add_reaction("❌")
        else:
            mv = 0
            for stonk in data["investors"][str(ctx.message.author.id)]["portfolio"]:
                ticker = yf.Ticker(stonk).info
                if float(ticker["bid"]) <= float(ticker["ask"]):
                    bid = float(ticker["bid"])
                else:
                    bid = float(ticker["ask"])
                mv += bid * int(data["investors"][str(ctx.message.author.id)]["portfolio"][stonk]["antall"])
            embed=discord.Embed(title=ctx.message.author, color=embedColour)
            embed.add_field(name="Tilgjengelige midler", value=str(int(data["investors"][str(ctx.message.author.id)]["balance"])) + " NOK", inline=False)
            embed.add_field(name="Markedsverdi", value=str(int(mv)) + " NOK", inline=False)
            ek = float(data["investors"][str(ctx.message.author.id)]["balance"]) + float(mv)
            embed.add_field(name="Egenkapital", value=str(int(ek)) + " NOK", inline=False)
            await ctx.send(embed=embed)
            data["investors"][str(ctx.message.author.id)]["ek"] = ek
            with open(status_file, "w", encoding='utf8') as f:
                json.dump(data,f)


    @commands.command()
    async def buy(self,ctx,antall,stonk):
        """Kjøper aksjen"""
        stonk = stonk.lower()
        data = get_investors()
        balance = float(data["investors"][str(ctx.message.author.id)]["balance"])
        ticker = yf.Ticker(stonk).info
        if ticker["bid"] > 0:
            if ticker["currency"] == "NOK":
                price = ticker["ask"] * int(antall)
                if price <= balance:
                    #print(ticker)
                    if stonk in data["investors"][str(ctx.message.author.id)]["portfolio"]:
                        data["investors"][str(ctx.message.author.id)]["portfolio"][stonk]["gav"] = ((data["investors"][str(ctx.message.author.id)]["portfolio"][stonk]["gav"] * data["investors"][str(ctx.message.author.id)]["portfolio"][stonk]["antall"]) + price) / (data["investors"][str(ctx.message.author.id)]["portfolio"][stonk]["antall"] + int(antall))
                        data["investors"][str(ctx.message.author.id)]["portfolio"][stonk]["antall"] += int(antall)
                        data["investors"][str(ctx.message.author.id)]["balance"] = balance - price
                    else:
                        data["investors"][str(ctx.message.author.id)]["portfolio"][stonk] = {"gav":ticker["ask"],"antall":int(antall)}
                        data["investors"][str(ctx.message.author.id)]["balance"] = balance - price
                    with open(status_file, "w", encoding='utf8') as f:
                        json.dump(data,f)
                    await ctx.send(f"Kjøper {antall} {ticker['shortName']} for {price} {ticker['currency']}")
                else:
                    await ctx.message.add_reaction("💸")
            else:
                await ctx.send("Kun mulig å handle på Oslo Børs")
        else:
            await ctx.send("Prisen er feil, vent til børsen åpner")

    @commands.command()
    async def sell(self,ctx,antall,stonk):
        """Selger aksjen"""
        stonk = stonk.lower()
        data = get_investors()
        ticker = yf.Ticker(stonk).info
        if stonk in data["investors"][str(ctx.message.author.id)]["portfolio"]:
            if int(antall) <= data["investors"][str(ctx.message.author.id)]["portfolio"][stonk]["antall"]:
                data["investors"][str(ctx.message.author.id)]["portfolio"][stonk]["antall"] -= int(antall)
                if float(ticker["bid"]) <= float(ticker["ask"]):
                    bid = float(ticker["bid"])
                else:
                    bid = float(ticker["ask"])
                data["investors"][str(ctx.message.author.id)]["balance"] += int(antall) * bid
                with open(status_file, "w", encoding='utf8') as f:
                    json.dump(data,f)
                await ctx.send(f"Selger {antall} {ticker['shortName']} for {int(antall) * ticker['bid']} {ticker['currency']}")
            else:
                await ctx.message.add_reaction("❌")

    @commands.command()
    async def portfolio(self,ctx):
        """Viser din portfolio"""
        data = get_investors()
        embed=discord.Embed(title=ctx.message.author, color=embedColour)
        if str(ctx.message.author.id) in data["investors"]:
            for stonk in data["investors"][str(ctx.message.author.id)]["portfolio"]:
                ticker = yf.Ticker(stonk).info
                mv = ticker["bid"] * data["investors"][str(ctx.message.author.id)]["portfolio"][stonk]["antall"]
                #embed.add_field(name="Markedsverdi", value=str(mv), inline=True)
                embed.add_field(name=stonk, value=str(data["investors"][str(ctx.message.author.id)]["portfolio"][stonk]["antall"]) + " (" + str(mv) + " NOK)", inline=True)
            await ctx.send(embed=embed)
        else:
            await ctx.send("Du har ikke noen account hos oss! Åpne med `€openaccount`")

def setup(bot):
    n = invest(bot)
    bot.add_cog(n)
