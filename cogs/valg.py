from discord.ext import commands
import json
import discord
import codecs
from os.path import expanduser
datafil = expanduser("~/credentials/valg.json")


class valg(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def stem(self, ctx, parti):
        """Stemmer på <parti>"""
        data = await self.les_data()
        stemmer = data["stemmer"]
        for party in data["stemmer"]:
            if ctx.author.id in stemmer[party]:
                await ctx.send(f"Du har allerede stemt på {party}, fjerner din stemme og prøver å stemme på et annet parti.")
                print(data["stemmer"][party.lower()])
                data["stemmer"][party.lower()].remove(ctx.author.id)

        if str(parti).lower() not in data["stemmer"]:
            return await ctx.send(parti + " stiller ikke liste her. Prøv igjen.")
        else:
            await ctx.send("En stemme til " + parti)
            nye_stemmer = stemmer[parti.lower()]
            nye_stemmer.append(ctx.author.id)
            await self.lagre_data(data)

    @commands.command()
    async def resultat(self, ctx, parti):
        """Viser antall stemmer <parti> har fått, samt oppslutning"""
        data = await self.les_data()
        votes = data["stemmer"]
        if parti.lower() not in data["stemmer"].keys():
            await ctx.send(parti + " stiller ikke liste her.")
            return
        else:
            numVotes = len(votes[parti.lower()])
            totalVotes = 0
            for party in data["stemmer"]:
                totalVotes = totalVotes + len(data["stemmer"][party])
            message = f"{numVotes} stemmer til {parti}. {str((numVotes/totalVotes)*100)} % oppslutning."  # Fstring er gøy
            await ctx.send(message)

    @commands.command()
    async def result(self, ctx, parti):
        """Viser oppslutning til <parti>"""
        data = await self.les_data()
        votes = data["stemmer"]
        if parti.lower() not in data["stemmer"].keys():
            await ctx.send(parti + " stiller ikke liste her.")
            return
        totalVotes = 0
        for party in data["stemmer"]:
            totalVotes = totalVotes + len(data["stemmer"][party])
        numVotes = len(votes[parti.lower()])
        message = str((numVotes/totalVotes)*100) + "%"
        await ctx.send(message)

    @commands.command()
    async def stilling(self, ctx):
        """Viser stillingen"""
        data = await self.les_data()
        totalVotes = 0
        for party in data["stemmer"]:
            totalVotes = totalVotes + len(data["stemmer"][party])
        message = ""
        for party in data["stemmer"]:
            stem = len(data["stemmer"][party])
            opp = (stem/totalVotes)*100
            message+=(str(stem) + " stemmer til " + str(party) + ". " + str(opp) + "%"+'\n')
#        await ctx.send(message)
        embed = discord.Embed(description=message, title="Valgstilling", color=0x50bdfe)
        await ctx.send(embed=embed)

    @commands.command()
    async def valgliste(self,ctx):
        """Viser valglista"""
        data = await self.les_data()
        await ctx.send(data["valgliste"])

    async def lagre_data(self, nydata):
        with open(datafil, 'w') as f:
            json.dump(nydata, f)

    async def les_data(self):
        with codecs.open(datafil, 'r', encoding='utf8') as f:
            data = json.load(f)
        return data

    async def total(self):
        data = await self.les_data()
        tot = 0
        for parti in data["stemmer"]:
            tot += 1
        return tot


def setup(bot):
    n = valg(bot)
    bot.add_cog(n)
