import time
from discord.ext import commands
import discord
import json
import random
import codecs
import functions
from os.path import expanduser

embedColour = 0x50bdfe
julaften = 1640376386
class jul(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def liste(self,ctx):
        """Legger deg til på listen over snille eller slemme barn"""
        functions.log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"command")
        with open(expanduser("~/credentials/liste.json"), 'r',encoding='utf8') as f:
            data = json.load(f)
            if (str(ctx.message.author.id) in data["slemme"]) or (str(ctx.message.author.id) in data["snille"]):
                await ctx.send("🎅: Du er alt på lista")
                return 0
            else:
                i = random.randint(1, 100)
                if i < 20:
                    data["slemme"].append(str(ctx.message.author.id))
                elif i > 20:
                    data["snille"].append(str(ctx.message.author.id))
        with open(expanduser("~/credentials/liste.json"), 'w',encoding='utf8') as f:
            json.dump(data,f)
        await ctx.send("🎅: La deg til på lista")

    @commands.command()
    async def listliste(self,ctx):
        """Lister lengden pa listene"""
        functions.log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"command")
        with open(expanduser("~/credentials/liste.json"), 'r',encoding='utf8') as f:
            data = json.load(f)
            await ctx.send("Snille : " + str(len(data["snille"])) + " Slemme : " + str(len(data["slemme"])))

    @commands.command()
    async def listgaver(self,ctx):
        """Lister lengden pa gavesekken"""
        functions.log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"command")
        with open(expanduser("~/credentials/liste.json"), 'r',encoding='utf8') as f:
            data = json.load(f)
            await ctx.send("Gaver : " + str(len(data["gave"])))

    @commands.command()
    async def giGave(self,ctx,gave):
        """Legger en gave i sekken til Julenissen"""
        functions.log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"command")
        message = str(gave) + " fra " + str(ctx.message.author)
        with open(expanduser("~/credentials/liste.json"), 'r',encoding='utf8') as f:
            data = json.load(f)
            if str(message) in data["gave"]:
                await ctx.send("🎅: Det er alt en slik gave i sekken")
                return 0
            elif "@" in str(message):
                await ctx.send("<:CarlsenPalm:330483537298063360>")
                return 0
        data["gave"].append(str(message))
        with open(expanduser("~/credentials/liste.json"), 'w',encoding='utf8') as f:
            json.dump(data,f)
            await ctx.message.add_reaction("🎅")
            await ctx.message.author.send("🎅: La " + message + " i sekken!")


    @commands.command()
    async def taGave(self,ctx):
        """Gir gaver til de snille barna"""
        functions.log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"command")
        if time.time() < julaften:
            await ctx.send("🎅: HoHo! Ingen gaver før julaften 21:00!")
            return 0
        elif time.time() > julaften:
            with open(expanduser("~/credentials/liste.json"), 'r',encoding='utf8') as f:
                data = json.load(f)
                if str(ctx.message.author.id) in  data["slemme"]:
                    await ctx.send("🎅: I år får du bare kull ⛏. Vennligst ikke brenn det  for å unngå global oppvarming.")
                elif str(ctx.message.author.id) in  data["snille"]:
                    gave=random.choice(data["gave"])
                    message = "🎅: I år får du " + gave
                    await ctx.send(message)
                else:
                    await ctx.send("🎅: HoHo! Ser ikke ut til at du er på lista mi.")

    @commands.command()
    async def tos(self,ctx):
        """Printer TOS"""
        functions.log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"command")
        await ctx.send("🎅: HoHo! Ved å legge gave i sekken med €giGave kommandoen, forplikter du å skaffe gaven. Den som får gaven har krav på å få den tilsendt fra deg innen nyttårsaften 2021. Hvis du ikke har råd til gaven du gav, kan du ta opp lån.")
#    @commands.command()
#    async def kalender(self,ctx,dag):
#        functions.log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"command")
#        with open(expanduser("~/credentials/kalender.json"), 'r',encoding='utf8') as f:
#            data = json.load(f)
#        try:
#            if str(dag)=='5':
#                await ctx.send("Halve kartet av en hel, munken vokter andre del. ```" + data["luker"][str(dag)][str(ctx.message.author.id)[-1:]] + "```")
#            else:
#                await ctx.send(data["luker"][dag])
#        except Exception as e:
#            await ctx.send("🎅: HoHo! En liten nisse du eller?")

#    @commands.command()
#    async def svar(self,ctx,dag,*,svar):
#        """Sjekker <dagens> <svar> i kalenderen!"""
#        functions.log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"command")
#        with open(expanduser("~/credentials/kalender.json"), 'r',encoding='utf8') as f:
#            data = json.load(f)
#        try:
#            if svar.lower() == str(data["svar"][dag]).lower():
#                await ctx.send("🎅: HoHo! Det ser ut til å stemme!")
#                if ctx.message.author.id not in data["scoreboard"][dag]:
#                    data["scoreboard"][dag].append(ctx.message.author.id)
#                    with open(expanduser("~/credentials/kalender.json"), 'w',encoding='utf8') as f:
#                        json.dump(data,f)
#            else:
#                await ctx.send("🎅: HoHo! Tror ikke det.")
#        except Exception as e:
#            await ctx.send("🎅: HoHo! Tror ikke det.")

#    @commands.command()
#    async def resultat(self,ctx,dag):
#        """Viser hvem som har klart <dags> oppgaven"""
#        functions.log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"command")
#        with open(expanduser("~/credentials/kalender.json"), 'r',encoding='utf8') as f:
#            data = json.load(f)
#        userlist = []
#        try:
#            for user in data["scoreboard"][dag]:
#                userlist.append('<@' + str(user) + '>')
#            tit='Scoreboard dag ' + dag
#            embed = discord.Embed(title=tit, color=embedColour)
#            embed.add_field(name='Luringer',     value=userlist, inline=True)
#            await ctx.send(embed=embed)
#        except Exception as e:
#            await ctx.send("Tilkall hjelp")

def setup(bot):
    n = jul(bot)
    bot.add_cog(n)
