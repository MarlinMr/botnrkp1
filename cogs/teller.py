from discord.ext import commands
import json
import codecs
import functions
from os.path import expanduser

class Events(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.Cog.listener()
    async def on_message(self, message):
        if (message.channel.id==655783376728358972) and (not message.author.bot):
            if message.content.isdigit():
                functions.log(message.created_at,message.author,message.content,message.channel,"teller")
                with open(expanduser("~/credentials/teller.json"), 'r',encoding='utf8') as f:
                    data = json.load(f)
                if str(data["leder"]) == str(message.author):
                    await message.author.send(message.content)
                elif int(message.content) == data["current"]+1:
                    data["current"]=int(message.content)
                    data["leder"] = str(message.author)
                    isPrime = True
                    #print("Number is " + message.content)
                    if (int(message.content) > 5):
                        #print("Number is larger than 3")
                        for i in range(2, int(message.content)//2):
                            #print(int(message.content) % i)
                            if ((int(message.content) % i) == 0):
                                #print("Not prime!")
                                isPrime = False
                                break
                    elif (int(message.content) == 4):
                        isPrime = False
                    if (isPrime):
                        if (str(message.author.id) in data["users"]):
                            data["users"][str(message.author.id)].append(int(message.content))
                        else:
                            data["users"][str(message.author.id)]=[int(message.content)]
                            data["index"][str(message.author.id)]=str(message.author)
                    with open(expanduser("~/credentials/teller.json"), 'w',encoding='utf8') as f:
                        json.dump(data,f)

def setup(bot):
    bot.add_cog(Events(bot))
