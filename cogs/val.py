import re
import urllib.request
from bs4 import BeautifulSoup
from discord.ext import commands

class val(commands.Cog):
	def __init__(self, bot):
		self.bot = bot
		
	@commands.command()
	async def odds(self,ctx):	
                html = urllib.request.Request(url='https://eurovisionworld.com/odds/eurovision', data=None,headers={'User-Agent': 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_9_3) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/35.0.1916.47 Safari/537.36'})
                soup = BeautifulSoup(html, "html.parser")
                result = str(soup)
                split = result.split('data-dt="4475"')
                await ctx.send(split[1][:5])
		
def setup(bot):
	 n = val(bot)
	 bot.add_cog(n)		
