from discord.ext import commands
import json
import discord
import codecs
import operator
from collections import Counter
from os.path import expanduser

#sf1 = 
sf2 = ['🇦🇲','🇮🇪','🇲🇩','🇨🇭','🇱🇻','🇷🇴','🇩🇰','🇸🇪','🇦🇹','🇭🇷','🇲🇹','🇱🇹','🇷🇺','🇦🇱','🇳🇴','🇳🇱','🇲🇰','🇦🇿']
finale = ['🇲🇹','🇦🇱','🇨🇿','🇩🇪','🇷🇺','🇩🇰','🇸🇲','🇲🇰','🇸🇪','🇸🇮','🇨🇾','🇳🇱','🇬🇷','🇮🇱','🇳🇴','🇬🇧','🇮🇸','🇪🇪','🇧🇾','🇦🇿','🇫🇷','🇮🇹','🇷🇸','🇨🇭','🇦🇺','🇪🇸']
finaleList={
    1:{"country":"Sør Norge",  "artist":"[Raylee](https://no.wikipedia.org/wiki/Raylee)"	                                            ,"song":"Wild"	                    ,"lang":"Dunno"		    },
    2:{"country":"Øst Norge",  "artist":"[Didrik](https://en.wikipedia.org/wiki/Didrik_Solli-Tangen) & [Emil Solli-Tangen](https://en.wikipedia.org/wiki/Emil_Solli-Tangen)"	                                            ,"song":"Out of Air"	                    ,"lang":"Dunno"		    },
    3:{"country":"Vest Norge",  "artist":"Magnus Bokn"	                                            ,"song":"Over the Sea"	                    ,"lang":"Dunno"		    },
    4:{"country":"Sentral Norge",   "artist":"[Akuvi](https://no.wikipedia.org/wiki/Akuvi)"	                                            ,"song":"Som du er"	                    ,"lang":"Dunno"		    },
    5:{"country":"Sentral Norge",   "artist":"[Kristin Husøy](https://no.wikipedia.org/wiki/Kristin_Hus%C3%B8y)"	                                            ,"song":"Pray For Me"	                    ,"lang":"Dunno"		    },
    6:{"country":"Øst Norge",       "artist":"[Rein Alexander](https://no.wikipedia.org/wiki/Rein_Alexander)"	                                            ,"song":"One Last Time"	                    ,"lang":"Dunno"		    },
    7:{"country":"Nord Norge",      "artist":"[Tone Damli](https://no.wikipedia.org/wiki/Tone_Damli)"	                                            ,"song":"Hurts Sometimes"	                    ,"lang":"Dunno"		    },
    8:{"country":"Nord Norge",      "artist":"[Sondrey](https://no.wikipedia.org/wiki/Sondre_Mulongo_Nystr%C3%B8m)"	                                            ,"song":"Take My Time"	                    ,"lang":"Dunno"		    },
    9:{"country":"Vest Norge",      "artist":"[Ulrikke Brandstorp](https://no.wikipedia.org/wiki/Ulrikke_Brandstorp)"	                                            ,"song":"Attention"	                    ,"lang":"Dunno"		    },
    10:{"country":"Nord Norge",     "artist":"Liza Vassilieva"	                                            ,"song":"I Am Gay"	                    ,"lang":"Dunno"		    },
}
#finaleList = {
#1:{"country":"🇲🇹",  "artist":"[Michela](https://en.wikipedia.org/wiki/Michela_Pace)"	                                            ,"song":"[Chameleon](https://en.wikipedia.org/wiki/Chameleon_(Michela_Pace_song\))"	                    ,"lang":"English"		    },
#2:{"country":"🇦🇱",  "artist":"[Jonida Maliqi](https://en.wikipedia.org/wiki/Jonida_Maliqi)"	                                    ,"song":"[Ktheju tokës](https://en.wikipedia.org/wiki/Ktheju_tok%C3%ABs)"	                            ,"lang":"Albanian"		    },
#3:{"country":"🇨🇿",  "artist":"[Lake Malawi](https://en.wikipedia.org/wiki/Lake_Malawi_(band\))"	                                ,"song":"[Friend of a Friend](https://en.wikipedia.org/wiki/Friend_of_a_Friend_(Lake_Malawi_song\))"    ,"lang":"English"		    },
#4:{"country":"🇩🇪",  "artist":"[S!sters](https://en.wikipedia.org/wiki/Sisters_(German_duo\))"	                                    ,"song":"[Sister](https://en.wikipedia.org/wiki/Sister_(S!sters_song\))"	                        ,"lang":"English"		    },
#5:{"country":"🇷🇺",  "artist":"[Lazarev](https://en.wikipedia.org/wiki/Sergey_Lazarev)"	                                        ,"song":"[Scream](https://en.wikipedia.org/wiki/Scream_(Sergey_Lazarev_song\))"	                        ,"lang":"English"		    },
#6:{"country":"🇩🇰",  "artist":"[Leonora](https://en.wikipedia.org/wiki/Leonora_(singer\))"	                                        ,"song":"[Love Is Forever](https://en.wikipedia.org/wiki/Love_Is_Forever_(Leonora_song\))"	        ,"lang":"English, French"   },
#7:{"country":"🇸🇲",  "artist":"[Serhat](https://en.wikipedia.org/wiki/Serhat_(singer\))"	                                        ,"song":"[Say Na Na Na](https://en.wikipedia.org/wiki/Say_Na_Na_Na)"	                                ,"lang":"English"		    },
#8:{"country":"🇲🇰",  "artist":"[Tamara Todevska](https://en.wikipedia.org/wiki/Tamara_Todevska)"	                                ,"song":"[Proud](https://en.wikipedia.org/wiki/Proud_(Tamara_Todevska_song\))"	                        ,"lang":"English"		    },
#9:{"country":"🇸🇪",  "artist":"[John Lundvik](https://en.wikipedia.org/wiki/John_Lundvik)"	                                    ,"song":"[Too Late for Love](https://en.wikipedia.org/wiki/Too_Late_for_Love_(John_Lundvik_song\))"	    ,"lang":"English"		    },
#10:{"country":"🇸🇮", "artist":"[Zala Kralj & Gašper Šantl](https://en.wikipedia.org/wiki/Zala_Kralj_%26_Ga%C5%A1per_%C5%A0antl)"  ,"song":"[Sebi](https://en.wikipedia.org/wiki/Sebi_(song\))"	                                        ,"lang":"Slovene"		    },
#11:{"country":"🇨🇾", "artist":"[Tamta](https://en.wikipedia.org/wiki/Tamta)"	                                                    ,"song":"[Replay](https://en.wikipedia.org/wiki/Replay_(Tamta_song\))"	                                ,"lang":"English"		    },
#12:{"country":"🇳🇱", "artist":"[Duncan Laurence](https://en.wikipedia.org/wiki/Duncan_Laurence)"	                                ,"song":"[Arcade](https://en.wikipedia.org/wiki/Arcade_(song\))"	                                    ,"lang":"English"		    },
#13:{"country":"🇬🇷", "artist":"[Katerine Duska](https://en.wikipedia.org/wiki/Katerine_Duska)"	                                ,"song":"[Better Love](https://en.wikipedia.org/wiki/Better_Love_(Katerine_Duska_song\))"	            ,"lang":"English"		    },
#14:{"country":"🇮🇱", "artist":"[Kobi Marimi](https://en.wikipedia.org/wiki/Kobi_Marimi)"	                                        ,"song":"[Home](https://en.wikipedia.org/wiki/Home_(Kobi_Marimi_song\))"	                            ,"lang":"English"		    },
#15:{"country":"🇳🇴", "artist":"[KEiiNO](https://en.wikipedia.org/wiki/Keiino)"	                                                ,"song":"[Spirit in the Sky](https://en.wikipedia.org/wiki/Spirit_in_the_Sky_(Keiino_song\))"	        ,"lang":"English"		    },
#16:{"country":"🇬🇧", "artist":"[Michael Rice](https://en.wikipedia.org/wiki/Michael_Rice_(singer\))"	                            ,"song":"[Bigger than Us](https://en.wikipedia.org/wiki/Bigger_than_Us_(Michael_Rice_song\))"	        ,"lang":"English"		    },
#17:{"country":"🇮🇸", "artist":"[Hatari](https://en.wikipedia.org/wiki/Hatari_(band\))"	                                            ,"song":"[Hatrið mun sigra](https://en.wikipedia.org/wiki/Hatri%C3%B0_mun_sigra)"	                ,"lang":"Icelandic"		    },
#18:{"country":"🇪🇪", "artist":"[Victor Crone](https://en.wikipedia.org/wiki/Victor_Crone)"	                                    ,"song":"[Storm](https://en.wikipedia.org/wiki/Storm_(Victor_Crone_song\))"	                            ,"lang":"English"		    },
#19:{"country":"🇧🇾", "artist":"[ZENA](https://en.wikipedia.org/wiki/Zinaida_Kupriyanovich)"	                                    ,"song":"[Like It](https://en.wikipedia.org/wiki/Like_It)"	                                            ,"lang":"English"	    	},
#20:{"country":"🇦🇿", "artist":"[Chingiz](https://en.wikipedia.org/wiki/Chingiz_Mustafayev_(singer\))"	                            ,"song":"[Truth](https://en.wikipedia.org/wiki/Truth_(Chingiz_song\))"	                                ,"lang":"English"		    },
#21:{"country":"🇫🇷", "artist":"[Bilal Hassani](https://en.wikipedia.org/wiki/Bilal_Hassani)"	                                    ,"song":"[Roi](https://en.wikipedia.org/wiki/Roi_(song\))"	                                            ,"lang":"French, English"   },
#22:{"country":"🇮🇹", "artist":"[Mahmood](https://en.wikipedia.org/wiki/Mahmood_(singer\))"	                                        ,"song":"[Soldi](https://en.wikipedia.org/wiki/Soldi)"	                                            ,"lang":"Italian"		    },
#23:{"country":"🇷🇸", "artist":"[Nevena Božović](https://en.wikipedia.org/wiki/Nevena_Bo%C5%BEovi%C4%87)"	                        ,"song":"[Kruna (Круна)](https://en.wikipedia.org/wiki/Kruna_(song\))"	                                ,"lang":"Serbian"		    },
#24:{"country":"🇨🇭", "artist":"[Luca Hänni](https://en.wikipedia.org/wiki/Luca_H%C3%A4nni)"	                                    ,"song":"[She Got Me](https://en.wikipedia.org/wiki/She_Got_Me)"	                                    ,"lang":"English"		    },
#25:{"country":"🇦🇺", "artist":"[Kate Miller-Heidke](https://en.wikipedia.org/wiki/Kate_Miller-Heidke)"	                        ,"song":"[Zero Gravity](https://en.wikipedia.org/wiki/Zero_Gravity_(Kate_Miller-Heidke_song\))"	        ,"lang":"English"		    },
#26:{"country":"🇪🇸", "artist":"[Miki](https://en.wikipedia.org/wiki/Miki_N%C3%BA%C3%B1ez)"	                                    ,"song":"[La Venda](https://en.wikipedia.org/wiki/La_venda)"	                                        ,"lang":"Spanish"	        }
#}
finaleNumbers = {"🇲🇹":1,"🇦🇱":2,"🇨🇿":3,"🇩🇪":4,"🇷🇺":5,"🇩🇰":6,"🇸🇲":7,"🇲🇰":8,"🇸🇪":9,"🇸🇮":10,"🇨🇾":11,"🇳🇱":12,"🇬🇷":13,"🇮🇱":14,"🇳🇴":15,"🇬🇧":16,"🇮🇸":17,"🇪🇪":18,"🇧🇾":19,"🇦🇿":20,"🇫🇷":21,"🇮🇹":22,"🇷🇸":23,"🇨🇭":24,"🇦🇺":25,"🇪🇸":26} 
esc = ['🇦🇱', '🇦🇲', '🇦🇺', '🇦🇹', '🇦🇿', '🇧🇾', '🇧🇪', '🇧🇦', '🇧🇬', '🇭🇷', '🇨🇾', '🇨🇿', '🇩🇪', '🇪🇪', '🇫🇮', '🇫🇷', '🇬🇪', '🇩🇪', '🇬🇷', '🇭🇺', '🇮🇸', '🇮🇪', '🇮🇱', '🇮🇹', '🇱🇻', '🇱🇹', '🇲🇹', '🇲🇩', '🇲🇪', '🇳🇱', '🇲🇰', '🇳🇴', '🇵🇱', '🇵🇹', '🇷🇴', '🇷🇺', '🇸🇲', '🇷🇸', '🇸🇰', '🇸🇮', '🇪🇸', '🇸🇪', '🇨🇭', '🇹🇷', '🇺🇦', '🇬🇧', '🇩🇰']
embedColour = 0x50bdfe
invert = lambda d: d.__class__(map(reversed, d.items()))
votelist = [12,10,8,7,6,5,4,3,2,1,0]

def log(time,user,message,channel,file):
    f= open(expanduser("~/credentials/") + file + ".log","a+")
    f.write(str(time) + " " + str(user) + " " + str(message) + " " + str(channel)+"\n")
    f.close()
    print(time,user,message,channel)
    
def standings():
    stillingen = {}
    with codecs.open(expanduser("~/credentials/eurovision.json"), 'r',encoding='utf8') as f:
        data = json.load(f)
    count = Counter({})
    for (_, voter) in data.items():
        inverted = invert(voter)
        inverted = {k: int(v) for k, v in inverted.items()}
        count += Counter(inverted)
    return(count)
    
    
def votes(points,country,author,time):
    with codecs.open(expanduser("~/credentials/eurovision.json"), 'r',encoding='utf8') as f:
            data = json.load(f)
    if points in data[str(author)]:
        itsVotes={}
        for index in list(data[str(author)]):
            if data[str(author)][index]==country or int(index)<1:
                del data[str(author)][index]
            elif int(index) > int(points):
                itsVotes[int(index)] = data[str(author)][index]
            elif int(index) in [12,10]:
                itsVotes[int(index)-2] = data[str(author)][index]
            elif (str(int(index)+1) in data[str(author)].keys()) or (int(index) == int(points)) or (str((int(index)+2) in data[str(author)].keys()) and (int(index)==8)):
                itsVotes[int(index)-1] = data[str(author)][index]
            else:
                itsVotes[int(index)] = data[str(author)][index]
        itsVotes[points] = country
        with codecs.open(expanduser("~/credentials/eurovision.json"), 'w',encoding='utf8') as f:
            sorted_keys = sorted(itsVotes.keys(),key=lambda key: int(key), reverse=True)
            sorted_dict = {}
            for i in sorted_keys:
                sorted_dict[i]=itsVotes[i]
            data[str(author)]=sorted_dict
            json.dump(data,f)
    elif country in data[str(author)].values():
        indexPoeng = invert(data[str(author)])[country]
        with open(expanduser("~/credentials/eurovision.json"), 'w',encoding='utf8') as f:
            data[str(author)].pop(indexPoeng)
            data[str(author)].update({points:country})
            sorted_keys = sorted(data[str(author)].keys(),key=lambda key: int(key), reverse=True)
            sorted_dict = {}
            for i in sorted_keys:
                sorted_dict[i]=data[str(author)][i]
            data[str(author)]=sorted_dict
            json.dump(data,f)
    else:
        if str(author) in data:
            with open(expanduser("~/credentials/eurovision.json"), 'w',encoding='utf8') as f:
                data[str(author)].update({points:country})
                sorted_keys = sorted(data[str(author)].keys(),key=lambda key: int(key), reverse=True)
                sorted_dict = {}
                for i in sorted_keys:
                    sorted_dict[i]=data[str(author)][i]
                data[str(author)]=sorted_dict
                json.dump(data,f)
    file="eurovisionfinale"
    f= open(expanduser("~/credentials/") + file + ".log","a+")
    f.write(str(time) + " " + str(standings()) + "\n")
    f.close()
    return(points + " points to " + country)
    
class eurovision(commands.Cog):
    def __init__(self, bot):
        self.bot = bot    
        
    @commands.command()
    async def eurohelp(self,ctx):
        """Prints helpfull message"""
        await ctx.send("This bot keeps track of discord votes on the Eurovision Song Contest. To be able to vote, you have to ``€enter``. You can only vote on 10 countries. You can only give 12, 10, 8, 7, 6, 5, 4, 3, 2, or 1 as points. You can change your votes at any time. Giving the same amount of points to the a new country, results in the newest vote pushing the other votes down the vote list. For syntaxt, ``€help eurovision``")
    
    @commands.command()
    async def songlist(self,ctx): 
        """Lists all the songs of the Finale, in order"""
        desc = ""
        for i in [1,2,3,4,5,6,7,8]:
            desc = desc + "\n ***" + str(i) + "*** " + finaleList[i]["country"] + " " + finaleList[i]["song"] + " sung by " + finaleList[i]["artist"] + " in " + finaleList[i]["lang"] 
        embed = discord.Embed(description=desc, title="Eurovision Finale", color=embedColour)
        desc = ""
        for i in [9,10]: #,11,12,13,14,15,16]:
            desc = desc + "\n ***" + str(i) + "*** " + finaleList[i]["country"] + " " + finaleList[i]["song"] + " sung by " + finaleList[i]["artist"] + " in " + finaleList[i]["lang"] 
        embed2 = discord.Embed(description=desc, color=embedColour)
        #desc = ""
        #for i in [17,18,19,20,21,22,23,24,25,26]:
        #    desc = desc + "\n ***" + str(i) + "*** " + finaleList[i]["country"] + " " + finaleList[i]["song"] + " sung by " + finaleList[i]["artist"] + " in " + finaleList[i]["lang"] 
        #embed3 = discord.Embed(description=desc, color=embedColour)
        await ctx.send(embed=embed)
        await ctx.send(embed=embed2)
        #await ctx.send(embed=embed3)
    
    @commands.command()
    async def vote(self,ctx,points,country: commands.clean_content):
        """Votes <points> on <country>"""
        log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"escvote")
        with open(expanduser("~/credentials/eurovision.json"), 'r',encoding='utf8') as f:
            data = json.load(f)
        if str(ctx.message.author.id) not in data:
            await ctx.send("You have not entered the voting. Please ``€enter`` first.")
        elif int(points) not in votelist:
            await ctx.send("You can only give 12, 10, 8, 7, 6, 5, 4, 3, 2, or 1 as points.")
        elif country not in finale:
            try:
                if 0 <= int(country) <= 10: #26:
                    await ctx.send(votes(points,finaleList[int(country)]['country'],ctx.message.author.id,ctx.message.created_at))
                else:
                    await ctx.send("This country is not in the Finale. You can only vote for " + str(finale))
            except Exception as e:
                if isinstance(country,str):
                    await ctx.send("This country is not in the Finale. You can only vote for " + str(finale))
                else:
                    await ctx.send("Voting error, contact <@202745416062599168>")
                
        else:
            await ctx.send(votes(points,country,ctx.message.author.id,ctx.message.created_at))
        
        
    @commands.command()
    async def myVotes(self,ctx):
        """Lists your votes"""
        with open(expanduser("~/credentials/eurovision.json"), 'r',encoding='utf8') as f:
            data = json.load(f)
        if str(ctx.message.author.id) in data:
            s = ""
            for index in data[str(ctx.message.author.id)].keys():
                s = s  + data[str(ctx.message.author.id)][index] + index + "   "
            desc = s
            embed = discord.Embed(description=desc, color=embedColour)
            await ctx.send(embed=embed)
        else:
            await ctx.send("You have not entered the voting. Please ``€enter`` first.")
        
    @commands.command()
    async def leaderboard(self,ctx):
        """Current leaderboard"""
        count=standings()
        desc = ('\n'.join([f"{i} with {n} points" for i, n in count.most_common()]))
        embed = discord.Embed(description=desc, title="Eurovision Finale", color=embedColour)
        await ctx.send(embed=embed)
                
    
    @commands.command()
    async def enter(self,ctx):
        """Enters you to the voting able list"""
        with open(expanduser("~/credentials/eurovision.json"), 'r',encoding='utf8') as f:
            data = json.load(f)
        if str(ctx.message.author.id) in data:
            await ctx.send("You have already entered.")
        else:
            with open(expanduser("~/credentials/eurovision.json"), 'w',encoding='utf8') as f:
                data.update({str(ctx.message.author.id):{}})
                await ctx.send("We have recieved your entry.")
                json.dump(data,f)
                
    @commands.command()
    async def song(self,ctx,ctrysongnum):
        """Shows information about the song"""
        if ctrysongnum in finale:
            embed = discord.Embed( title="Eurovision Finale", color=embedColour)
            land = finaleList[finaleNumbers[ctrysongnum]]["country"]
            artist = finaleList[finaleNumbers[ctrysongnum]]["artist"]
            song = finaleList[finaleNumbers[ctrysongnum]]["song"]
            lang = finaleList[finaleNumbers[ctrysongnum]]["lang"]
            txt = f"**{finaleNumbers[ctrysongnum]}** {land} "
            txt_sang = f"{song} sung by {artist} in {lang}"
            embed.add_field(name=txt, value=txt_sang, inline=False)
            await ctx.send(embed=embed)
        elif 0 < int(ctrysongnum) <= 26:
            embed = discord.Embed( title="Eurovision Finale", color=embedColour)
            land = finaleList[int(ctrysongnum)]["country"]
            artist = finaleList[int(ctrysongnum)]["artist"]
            song = finaleList[int(ctrysongnum)]["song"]
            lang = finaleList[int(ctrysongnum)]["lang"]
            txt = f"**{ctrysongnum}** {land} "
            txt_sang = f"{song} sung by {artist} in {lang}"
            embed.add_field(name=txt, value=txt_sang, inline=False)
            await ctx.send(embed=embed)
        else:
            await ctx.send("This country is not in the Finale. Try " + str(finale) + " instead.")
    
    
    @commands.command()
    async def stream(self,ctx):
        """Norwegian Stream"""
        await ctx.send("https://tv.nrk.no/direkte/nrk1")
    
    
def setup(bot):
    n = eurovision(bot)
    bot.add_cog(n)
    