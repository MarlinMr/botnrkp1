from discord.ext import commands
import discord
import miio
import json
import codecs
from os.path import expanduser

with open(expanduser("~/credentials/vacuum.json"), 'r',encoding='utf8') as f:
    data = json.load(f)
    ip=data["ip"]
    Token=data["token"]

v = miio.vacuum.Vacuum(ip,token=Token)

class vacuum(commands.Cog):

    def __init__(self,bot):
        self.bot=bot

    @commands.command()
    async def startSug(self,ctx):
        """Starter støvsugeren hos Marlin"""
        log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"vacuum.command")
        try:
            v.start()
        except Exception as e:
            print(e)
            log(ctx.message.created_at,ctx.message.author,ctx.message.content,e,"vacuum.error")
        await ctx.send("Suger støv!▶️🌬️")

    @commands.command()
    async def stopSug(self,ctx):
        """Stopper støvsugeren hos Marlin"""
        log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"vacuum.command")
        try:
            v.stop()
        except Exception as e:
            print(e)
            log(ctx.message.created_at,ctx.message.author,ctx.message.content,e,"vacuum.error")
        await ctx.send("🛑🌬️")

    @commands.command()
    async def pauseSug(self,ctx):
        """Pauser støvsugeren hos Marlin"""
        log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"vacuum.command")
        try:
            v.pause()
        except Exception as e:
            print(e)
            log(ctx.message.created_at,ctx.message.author,ctx.message.content,e,"vacuum.error")
        await ctx.send("⏸️🌬️")

    @commands.command()
    async def hjemSug(self,ctx):
        """Sender støvsugeren til Marlin hjem til ladestasjonen"""
        log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"vacuum.command")
        try:
            v.home()
        except Exception as e:
            print(e)
            log(ctx.message.created_at,ctx.message.author,ctx.message.content,e,"vacuum.error")
        await ctx.send("🚶🏡⚡🔌")

    @commands.command()
    async def finnSug(self,ctx):
        """Får støvsugeren til Marlin til å rope ut hvor den er"""
        log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"vacuum.command")
        try:
            v.find()
        except Exception as e:
            print(e)
            log(ctx.message.created_at,ctx.message.author,ctx.message.content,e,"vacuum.error")
        await ctx.send("EG ER HER BORTE 🔊")

    @commands.command()
    async def blowJob(self,ctx):
        """( ͡° ͜ʖ ͡°)"""
        log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"vacuum.command")
        try:
            v.start()
        except Exception as e:
            print(e)
            log(ctx.message.created_at,ctx.message.author,ctx.message.content,e,"vacuum.error")
        await ctx.send("🌬️")

    @commands.command()
    async def volumSug(self,ctx):
        """Tester volumet til støvsugeren til Marlin"""
        log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"vacuum.command")
        try:
            v.test_sound_volume()
        except Exception as e:
            print(e)
            log(ctx.message.created_at,ctx.message.author,ctx.message.content,e,"vacuum.error")
        await ctx.send("Testing 🔈")

def log(time,user,message,channel,file):
    f= open(expanduser("~/credentials/") + file + ".log","a+")
    f.write(str(time) + " " + str(user) + " " + str(message) + " " + str(channel)+"\n")
    f.close()

def setup(bot):
    n = vacuum(bot)
    bot.add_cog(n)
