from discord.ext import commands
import discord

class radio(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        
    @commands.command()
    async def radio(self, ctx, query):
        try:
            voiceChannel = ctx.message.author.voice.channel
            await voiceChannel.connect()
            source = discord.PCMVolumeTransformer(discord.FFmpegPCMAudio(query))
            voiceChannel.play(source)
        except Exception as e:
            await ctx.send(e)
        
def setup(bot):
    bot.add_cog(radio(bot))