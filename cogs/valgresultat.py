from discord.ext import commands
import os
import discord
import json
import codecs
import socket
import requests
import subprocess
import functions
from os.path import expanduser
embedColour = 0x50bdfe
current_year=2023
partilogo = {
    "A":"https://upload.wikimedia.org/wikipedia/commons/thumb/c/c3/Arbeiderpartiet_logo.svg/223px-Arbeiderpartiet_logo.svg.png",
    "SV":"https://www.sv.no/wp-content/uploads/2021/03/Logo-300x177-1.png",
    "KRF":"https://krf.no/content/uploads/2020/10/krf_logo2017-1.png",
    "FRP":"https://media.snl.no/media/33512/standard_FrP_eple_NEG.png",
    "RØDT":"https://upload.wikimedia.org/wikipedia/commons/thumb/1/14/R%C3%B8dt_logo_%28bokm%C3%A5l%29.svg/222px-R%C3%B8dt_logo_%28bokm%C3%A5l%29.svg.png",
    "MDG":"https://upload.wikimedia.org/wikipedia/commons/thumb/5/53/Milj%C3%B8partiet_de_Gr%C3%B8nne_logo.svg/320px-Milj%C3%B8partiet_de_Gr%C3%B8nne_logo.svg.png",
    "V":"https://www.venstre.no/assets/Venstre-logo-symbol.png",
    "SP":"https://www.senterpartiet.no/presse/grafisk-profil/_/image/fefda4d3-9ac8-41bf-a9fe-8a79838cc3df:356ba70ae28f43b95465a375b5bf0cedf52adc36/block-161-121/2020%20hovudlogo%20kvit%20RGB%201200px.png",
    "H":"https://res.cloudinary.com/hoyre/images/f_auto,q_auto/v1607432632/Nye%20hoyre.no/Hovedside/1_start_hoyre_no_visuell_identitet_125197eec5/1_start_hoyre_no_visuell_identitet_125197eec5.png",
    "ALLI":"http://www.stemalliansen.no/wp-content/uploads/2021/02/ALLOGO-SUN-200x200.png",
    "DEMN":"https://secureservercdn.net/160.153.137.40/ym4.01e.myftpupload.com/wp-content/uploads/2020/07/logo-1.png",
    "HELSE":"https://helsepartiet.no/wp-content/uploads/2021/06/HP_bredde_friskere.png",
    "KRISTNE":"https://upload.wikimedia.org/wikipedia/commons/thumb/9/93/Partiet_De_Kristne.svg/320px-Partiet_De_Kristne.svg.png",
    "KYST":"https://upload.wikimedia.org/wikipedia/commons/thumb/0/0d/Kystpartiet_logo.png/240px-Kystpartiet_logo.png",
    "LIBS":"https://upload.wikimedia.org/wikipedia/commons/3/3e/Liberalistene_logo.png",
    "NKP":"https://media.snl.no/media/36230/standard_nkp.png",
    "PIR":"https://www.piratpartiet.no/wp-content/uploads/2018/05/Pir-flagglogo-580x580.jpg",
    "PP":"https://media.snl.no/media/36232/standard_pp.png",
    "SAMFS":"https://upload.wikimedia.org/wikipedia/en/c/ca/Samfunnspartiet_logo.png",
    "FI":"https://www.feministiskinitiativ.no/wp-content/uploads/2016/06/cropped-fav-1.png",
    "NML":"https://upload.wikimedia.org/wikipedia/commons/thumb/c/cd/Logo-Nordmorslista.png/254px-Logo-Nordmorslista.png",
    "NORDT":"https://upload.wikimedia.org/wikipedia/commons/b/ba/Nordting_logo.jpg",
    "NP":"https://www.norgespartiet.no/wp-content/uploads/2020/12/norgespartiet_logo_nye_farger-ok-kvalitet-transparent.png",
    "VERDI":"https://www.verdipartiet.no/images/stemvp.png"
 }

class valgresultat(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    async def get_total_results(self, ctx, year, type, valgtype):
        try:
            api_url = f"https://valgresultat.no/api/{year}/{type}"
            response = requests.get(api_url, headers={'User-Agent': 'Mozilla/5.0'})
            embed=discord.Embed()
            embed=discord.Embed(title=f"{valgtype} {response.json()['id']['valgaar']}", description=f"{response.json()['opptalt']['forelopig']}% opptelte stemmer")
            for parti in response.json()["partier"]:
                prosent = round(parti["stemmer"]["resultat"]["prosent"],1)
                endring = round(parti["stemmer"]["resultat"]["endring"]["samme"], 1)
                p = parti["id"]["navn"]
                if (endring>0):
                    p = '⬆️' + str(p)
                elif (endring<0):
                    p = '⬇️' + str(p)
                mandat  = parti["mandater"]["resultat"]["antall"]
                dmandat = parti["mandater"]["resultat"]["endring"]
                embed.add_field(name=p, value=f'{prosent} % ({endring} %)\n {mandat} MPs ({dmandat})', inline=True)
            await ctx.send(embed=embed)
        except Exception as e:
            await ctx.message.add_reaction("❌")
            await ctx.send(f"{current_year} resultateer er ikke tilgjengelig før 21:00")
            print(e)

    async def get_total_results_ko(self, ctx, year, type, valgtype, ko):
        try:
            api_url = f"https://valgresultat.no/api/{year}/{type}/{ko}"
            response = requests.get(api_url, headers={'User-Agent': 'Mozilla/5.0'})
            embed=discord.Embed()
            embed=discord.Embed(title=f"{valgtype} {response.json()['id']['valgaar']} i {ko}", description=f"{response.json()['opptalt']['forelopig']}% opptelte stemmer")
            for parti in response.json()["partier"]:
                prosent = round(parti["stemmer"]["resultat"]["prosent"],1)
                endring = round(parti["stemmer"]["resultat"]["endring"]["samme"], 1)
                p = parti["id"]["navn"]
                if (endring>0):
                    p = '⬆️' + str(p)
                elif (endring<0):
                    p = '⬇️' + str(p)
                mandat  = parti["mandater"]["resultat"]["antall"]
                dmandat = parti["mandater"]["resultat"]["endring"]
                embed.add_field(name=p, value=f'{prosent} % ({endring} %)\n {mandat} MPs ({dmandat})', inline=True)
            await ctx.send(embed=embed)
        except Exception as e:
            await ctx.message.add_reaction("❌")
            await ctx.send(f"{current_year} resultateer er ikke tilgjengelig før 21:00")
            print(e)

    async def get_total_results_fy(self, ctx, year, type, valgtype, fy):
        print("hello")
        try:
            api_url = f"https://valgresultat.no/api/{year}/{type}/{fy}"
            response = requests.get(api_url, headers={'User-Agent': 'Mozilla/5.0'})
            embed=discord.Embed()
            embed=discord.Embed(title=f"{valgtype} {response.json()['id']['valgaar']}", description=f"{response.json()['opptalt']['forelopig']}% opptelte stemmer")
            for parti in response.json()["partier"]:
                prosent = round(parti["stemmer"]["resultat"]["prosent"],1)
                endring = round(parti["stemmer"]["resultat"]["endring"]["samme"], 1)
                p = parti["id"]["navn"]
                if (endring>0):
                    p = '⬆️' + str(p)
                elif (endring<0):
                    p = '⬇️' + str(p)
                mandat  = parti["mandater"]["resultat"]["antall"]
                dmandat = parti["mandater"]["resultat"]["endring"]
                embed.add_field(name=p, value=f'{prosent} % ({endring} %)\n {mandat} MPs ({dmandat})', inline=True)
            await ctx.send(embed=embed)
        except Exception as e:
            await ctx.message.add_reaction("❌")
            await ctx.send(f"{current_year} resultateer er ikke tilgjengelig før 21:00")
            print(e)

    @commands.command()
    async def storting(self, ctx, year=current_year):
        """Viser resultatet for Stortingsvalget i <år>"""
        await valgresultat.get_total_results(self=self, ctx=ctx, year=year, type='st', valgtype='Stortingsvalget')

    @commands.command()
    async def fylkesting(self, ctx, fylke, year=current_year):
        """Viser resultatet for Fylkestingsvalget i <år>"""
        await valgresultat.get_total_results_fy(self=self, ctx=ctx, year=year, type='fy', valgtype='Fylkestingsvalget', fy=fylke)

    @commands.command()
    async def kommunestyre(self, ctx, kommune, year=current_year):
        """Viser resultatet for Kommunestyret i <år>"""
        await valgresultat.get_total_results_ko(self=self, ctx=ctx, year=year, type='ko', valgtype='Kommunestyrevalget', ko=kommune)

    @commands.command()
    async def sameting(self, ctx, year=current_year):
        """Viser resultatet for Fylkestingsvalget i <år>"""
        await valgresultat.get_total_results(self=self, ctx=ctx, year=year, type='sa', valgtype='Sametingssvalget')
 
    @commands.command()
    async def parti(self, ctx, party, year=current_year, type=None):
        """Viser resultatet for <partiet> i <år>"""
        try:
            if (type==None):
                if ((int(year)-1)%4==0):
                    type='st'
                else:
                    type='fy'
            api_url = f"https://valgresultat.no/api/{year}/{type}"
            response = requests.get(api_url, headers={'User-Agent': 'Mozilla/5.0'})
            embed=discord.Embed()
            for parti in response.json()["partier"]:
                p = parti["id"]["navn"]
                if ((p.lower()==party.lower()) or parti["id"]["partikode"]==party.upper()):
                    prosent = round(parti["stemmer"]["resultat"]["prosent"],1)
                    endring = round(parti["stemmer"]["resultat"]["endring"]["samme"], 1)
                    opp="Oppslutning"
                    md="Mandater"
                    if (endring>0):
                        opp = '⬆️Oppslutning'
                    elif (endring<0):
                        opp = '⬇️Oppslutning'
                    mandat  = parti["mandater"]["resultat"]["antall"]
                    if (endring>0):
                        md = '⬆️Mandater'
                    elif (endring<0):
                        md = '⬇️Mandater'
                    dmandat = parti["mandater"]["resultat"]["endring"]
                    embed=discord.Embed(title=f"{p} ({response.json()['id']['valgaar']})", description=f"{response.json()['opptalt']['forelopig']}% opptelte stemmer")
                    embed.add_field(name=opp, value=f'{prosent} % \n ({endring} %)', inline=True)
                    embed.add_field(name=md, value=f'{mandat} \n ({dmandat})', inline=True)
                    stemmer=parti["stemmer"]["resultat"]["antall"]["total"]
                    fhs=parti["stemmer"]["resultat"]["antall"]["fhs"]
                    fhs_ratio=int(fhs)/int(stemmer)
                    fhs_ratio=fhs_ratio*100
                    embed.add_field(name="Stemmer", value=f'{stemmer}', inline=True)
                    embed.add_field(name="Forhåndsstemmer", value=f'{fhs} ({round(fhs_ratio,1)} %)', inline=True)
                    try:
                        embed.set_thumbnail(url=partilogo[parti["id"]["partikode"]])
                    except Exception as ThumbnailError:
                        print(ThumbnailError)
                    await ctx.send(embed=embed)
        except Exception as e:
            await ctx.message.add_reaction("❌")
            await ctx.send(f"{current_year} resultateer er ikke tilgjengelig før 21:00")
            print(e)

    @commands.command()
    async def parti_alle(self, ctx, year=current_year, type=None):
        """Viser resultatet for <partiet> i <år>"""
        try:
            if (type==None):
                if ((int(year)-1)%4==0):
                    type='st'
                else:
                    type='fy'
            api_url = f"https://valgresultat.no/api/{year}/{type}"
            response = requests.get(api_url, headers={'User-Agent': 'Mozilla/5.0'})
            for parti in response.json()["partier"]:
                p = parti["id"]["navn"]
                embed=discord.Embed()
                prosent = round(parti["stemmer"]["resultat"]["prosent"],1)
                endring = round(parti["stemmer"]["resultat"]["endring"]["samme"], 1)
                if (endring>0):
                    opp = '⬆️Oppslutning'
                elif (endring<0):
                    opp = '⬇️Oppslutning'
                mandat  = parti["mandater"]["resultat"]["antall"]
                if (endring>0):
                    md = '⬆️Mandater'
                elif (endring<0):
                    md = '⬇️Mandater'
                mandat  = parti["mandater"]["resultat"]["antall"]
                dmandat = parti["mandater"]["resultat"]["endring"]
                embed=discord.Embed(title=f"{p} ({response.json()['id']['valgaar']})", description=f"{response.json()['opptalt']['forelopig']}% opptelte stemmer")
                embed.add_field(name=opp, value=f'{prosent} % \n ({endring} %)', inline=True)
                embed.add_field(name=md, value=f'{mandat} \n ({dmandat})', inline=True)
                stemmer=parti["stemmer"]["resultat"]["antall"]["total"]
                fhs=parti["stemmer"]["resultat"]["antall"]["fhs"]
                fhs_ratio=int(fhs)/int(stemmer)
                fhs_ratio=fhs_ratio*100
                embed.add_field(name="Stemmer", value=f'{stemmer}', inline=True)
                embed.add_field(name="Forhåndsstemmer", value=f'{fhs} ({round(fhs_ratio,1)} %)', inline=True)
                try:
                    embed.set_thumbnail(url=partilogo[parti["id"]["partikode"]])
                except Exception as ThumbnailError:
                    print(ThumbnailError)
                await ctx.send(embed=embed)
        except Exception as e:
            await ctx.message.add_reaction("❌")
            await ctx.send(f"{current_year} resultateer er ikke tilgjengelig før 21:00")
            print(e)

    @commands.command()
    async def partiliste(self, ctx, year=current_year, type=None):
        """Viser partier og partikoder"""
        try:
            if (type==None):
                if ((int(year)-1)%4==0):
                    type='st'
                else:
                    type='fy'
            api_url = f"https://valgresultat.no/api/{year}/{type}"
            response = requests.get(api_url, headers={'User-Agent': 'Mozilla/5.0'})
            embed=discord.Embed()
            embed=discord.Embed(title=f"Partiliste ({response.json()['id']['valgaar']})", description=f"")
            for parti in response.json()["partier"]:
                p = parti["id"]["navn"]
                embed.add_field(name=p, value=f'{parti["id"]["partikode"]}', inline=True)
            await ctx.send(embed=embed)
        except Exception as e:
            await ctx.message.add_reaction("❌")
            await ctx.send(f"{current_year} resultateer er ikke tilgjengelig før 21:00")
            print(e)

def setup(bot):
    n = valgresultat(bot)
    bot.add_cog(n)
