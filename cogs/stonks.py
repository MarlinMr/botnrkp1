from discord.ext import commands
import os
import discord
import json
import codecs
import yfinance as yf
embedColour = 0x50bdfe


class stonks(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def stonk(self,ctx,stonk):
        """Stonk go up, stonk go down. You can't explain that"""
        ticker = yf.Ticker(stonk).info
        change = ((ticker["ask"]/ticker["previousClose"])-1)*100
        if (change > 10):
            change =   '🚀 ' + str(change)
        elif (change < 0):
            change = '📉 ' + str(change) 
        embed=discord.Embed(title=ticker["shortName"], color=embedColour, description=ticker["currency"])
        embed.set_thumbnail(url=ticker["logo_url"])
        embed.add_field(name="Ask", value=ticker["ask"], inline=True)
        embed.add_field(name="Bid", value=ticker["bid"], inline=True)
        embed.add_field(name="Low", value=ticker["dayLow"], inline=True)
        embed.add_field(name="High", value=ticker["dayHigh"], inline=True)
        embed.add_field(name="LastDay", value=ticker["previousClose"], inline=True)
        embed.add_field(name="Change", value=change, inline=True)
        await ctx.send(embed=embed)

def setup(bot):
    n = stonks(bot)
    bot.add_cog(n)
