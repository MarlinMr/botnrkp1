from discord.ext import commands
import json
import discord
import requests
class nve(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.source = []

    @commands.command()
    async def omradetyper(self, ctx):
        """Viser alle områdenetyper"""
        response = requests.get("https://nvebiapi.nve.no/api/Magasinstatistikk/HentOmråder")
        text = ""
        for element in response.json():
            text = text + f'`{element}`, type: `{response.json()[element][0]["omrType"]}`\n'
        await ctx.send(text)

    @commands.command()
    async def omrade(self,ctx,type):
        """Gir informasjon om <områdetype> (land|elspot|vass)"""
        response = requests.get("https://nvebiapi.nve.no/api/Magasinstatistikk/HentOmråder")
        text=""
        for element in response.json()[type.lower()]:
            text = text + f'`{element["navn_langt"]}` : {element["beskrivelse"]}.\n' 
        await ctx.send(text)

        

    @commands.command()
    async def fyllingsgrad(self,ctx,omrade,type):
        """PrINTER fyllingsgraden i <område> av <type>"""
        response = requests.get("https://nvebiapi.nve.no/api/Magasinstatistikk/HentOffentligDataSisteUke")
        for element in response.json():
            if ((int(element["omrnr"]) == int(omrade)) and (type.lower() == element["omrType"].lower())):
                embed=discord.Embed(title="Fyllingsgrad", description="Område " + str(element["omrnr"]) + " uke " + str(element["iso_uke"]))
                embed.set_thumbnail(url="https://www.nve.no/media/12523/nve_logo_firk_red_white.svg")
                fylgrad = str(round(100*element["fyllingsgrad"], 1)) + "%"
                kapasitet = str(round(element["kapasitet_TWh"], 1)) + "TWh"
                fylling = str(round(element["fylling_TWh"], 1)) + "TWh"
                endring = str(round(element["endring_fyllingsgrad"]*100, 1)) + "%"
                if element["endring_fyllingsgrad"] > 0:
                    endring = "⬆️  "+ endring
                else:
                    endring = "⬇️  " + endring

                embed.add_field(name="Fyllingsgrad", value=fylgrad, inline=True)
                embed.add_field(name="Kapasitet", value=kapasitet, inline=True)
                embed.add_field(name="Fylling", value=fylling, inline=True)
                embed.add_field(name="Endring fra forrige uke", value=endring, inline=True)
                await ctx.send(embed=embed)
                break


def setup(bot):
    n = nve(bot)
    bot.add_cog(n)
