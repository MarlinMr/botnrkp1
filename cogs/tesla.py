from discord.ext import commands
import json
import time
import discord
import codecs
import teslajson
embedColour = 0x50bdfe
from os.path import expanduser


with open(expanduser("~/credentials/tesla.json"), 'r',encoding='utf8') as f:
    data = json.load(f)

username=data["username"]
password=data["password"]
id=data["id"]

def isOnline():
    with teslapy.Tesla(username,password) as tesla:
        c = teslajson.Connection('youremail', 'yourpassword')
        v = c.vehicles[0]
        tesla.fetch_token()
        vehicles = tesla.vehicle_list()
    if vehicles[0]['state'] == 'online':
        return True;
    else:
        return False;

class Tesla(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        
    @commands.command()
    async def teslaStatus(self,ctx):
        """Viser status på Marlin sin Tesla"""
        if isOnline():
            with teslapy.Tesla(username,password) as tesla:
                tesla.fetch_token()
                vehicles = tesla.vehicle_list()
            data = vehicles[0].get_vehicle_data()
            power = data['drive_state']['power']
            power = str(power) + " kW"
            speed = data['drive_state']['speed']
            if not speed:
                speed = 0
            speed = speed*1.60934
            speed = str(round(speed)) + " km/h"
            sstate = data['drive_state']['shift_state']
            if not sstate:
                sstate = 'Parked'
            
            heading = data['drive_state']['heading']
            heading = str(heading) + "°N"
            #lat = data['drive_state']['latitude'] + data['drive_state']['longitude']
            lat = data['drive_state']['latitude']
            pos = 'https://google.com/maps/@' + str(lat)
            lat = str(lat) + "°N"
            #long = data['drive_state']['longitude'] + data['drive_state']['latitude']
            long = data['drive_state']['longitude']
            pos = pos + ',' + str(long) + ',19z'
            long = str(long) + "°E"
            
            soc = data['charge_state']['battery_level']
            soc = str(soc) + "%"
            irange = data['charge_state']['ideal_battery_range']
            irange = str(round(irange*1.60934)) + " km"
            erange = data['charge_state']['est_battery_range']
            erange = str(round(erange*1.60934)) + " km"
            
            otemp = data['climate_state']['outside_temp']
            otemp = str(otemp) + "°C"
            itemp = data['climate_state']['inside_temp']
            itemp = str(itemp) + "°C"
            stemp = data['climate_state']['driver_temp_setting']
            stemp = str(stemp) + "°C"
            
            embed = discord.Embed(title='Marlins Tesla', color=embedColour)
            embed.add_field(name='State of Charge',     value=soc, inline=True)
            embed.add_field(name='Ideal range',         value=irange, inline=True)
            embed.add_field(name='Estimated range',     value=erange, inline=True)
            
            
            current = str(data['charge_state']['charger_actual_current']) + "/" + str(data['charge_state']['charge_current_request_max']) + "A"
            voltage = str(data['charge_state']['charger_voltage']) + "V"
            remaining = str(data['charge_state']['minutes_to_full_charge']) + ' minutes'
            
            embed.add_field(name='Current',         value=current, inline=True)
            embed.add_field(name='Voltage',         value=voltage, inline=True)
            embed.add_field(name='Remaining',       value=remaining, inline=True)
            
            energy = str(data['charge_state']['charge_energy_added']) + " kWh"
            crate = str(round(data['charge_state']['charge_rate']*1.60934)) + " km/h"
            arange = str(data['charge_state']['charge_miles_added_ideal']*1.60934) + " km"
            
            embed.add_field(name='Added energy',            value=energy, inline=True)
            embed.add_field(name='Charge rate',             value=crate, inline=True)
            embed.add_field(name='Added range',             value=arange, inline=True)
            
            embed.add_field(name='Power',               value=power, inline=True)
            embed.add_field(name='Speed',               value=speed, inline=True)
            embed.add_field(name='Shift state',         value=sstate, inline=True)
            
            
            embed.add_field(name='Outside temp',        value=otemp, inline=True)
            embed.add_field(name='Inside temp',         value=itemp, inline=True)
            embed.add_field(name='Selected temp',       value=stemp, inline=True)
            
            
            embed.add_field(name='Latitude',            value=lat, inline=True)
            embed.add_field(name='Longitude',           value=long, inline=True)
            embed.add_field(name='Heading',             value=heading, inline=True)
            
            software = str(data['vehicle_state']['car_version'])
            cstate = data['charge_state']['charging_state']
            odometer = data['vehicle_state']['odometer']
            odometer = str(round(odometer*1.60934)) + " km"
            
            embed.add_field(name='Odometer',            value=odometer, inline=True)
            embed.add_field(name='Charging state',      value=cstate, inline=True)
            embed.add_field(name='Software',            value=software, inline=True)

            embed.add_field(name='Live map',                 value="https://marlinmr.se/tesla/index.php", inline=True) 
            await ctx.send(embed=embed)
        else:
            await ctx.send("Bilen sover")
        
        
    @commands.command()
    async def retning(self,ctx):
        """Viser kompassretningen til bilen"""
        if isOnline():
            with teslapy.Tesla(username,password) as tesla:
                tesla.fetch_token()
                vehicles = tesla.vehicle_list()
            car = vehicles[0].get_vehicle_data()
            await ctx.send(str(car['drive_state']['heading']))
        else:
            await ctx.send("Bilen sover")
    
    @commands.command()
    async def fart(self,ctx):
        """Viser farten og kraften til bilen"""
        if isOnline():
            with teslapy.Tesla(username,password) as tesla:
                tesla.fetch_token()
                vehicles = tesla.vehicle_list()
            data = vehicles[0].get_vehicle_data()
            power = data['drive_state']['power']
            speed = data['drive_state']['speed']
            if not speed:
                speed = 0
            speed = speed*1.60934
            await ctx.send(str(speed) + " kmh og presser på " + str(power) + " kW" )
        else:
            await ctx.send("Bilen sover")

        
    @commands.command()
    async def SoC(self,ctx):
        """Viser batteriets State of Charge"""
        if isOnline():
            with teslapy.Tesla(username,password) as tesla:
                tesla.fetch_token()
                vehicles = tesla.vehicle_list()
            data = vehicles[0].get_vehicle_data()
            charge = data['charge_state']
            await ctx.send(':battery:' + str(charge['battery_level'])+'% ('+str(charge['battery_range']*1.609) + 'km)')
        else:
            await ctx.send("Bilen sover")

#        if(!isOnline()):
#            await ctx.send('Bilen sover')
#        else:
#            try:
#                with teslapy.Tesla(username,password) as tesla:
#                    tesla.fetch_token()
#                    vehicles = tesla.vehicle_list()
#                    data = vehicles[0].get_vehicle_data()
#                charge = data['charge']
#                await ctx.send(':battery:' + str(charge['battery_level'])+'% ('+str(charge['battery_range']*1.609) + 'km)')
#            except Exception as e:
#                print(e)


#    @commands.command()
#    async def vekk(self,ctx):
#        """Vekker bilen"""
#        vehicles = client.list_vehicles()
#        for v in vehicles:
#            if(v.state == 'asleep'):
#                v.wake_up()
#                if(v.state=='asleep'):
#                    await ctx.send('Kunne ikke vekke bilen')
#            elif(v.state == 'online'):
#                await ctx.send('Den er alt våken...')

#    @commands.command()
#    async def klima(self,ctx):
#        """Viser lokalt klima"""
#        vehicles = client.list_vehicles()
#        for v in vehicles:
#            if(v.state == 'asleep'):
#                await ctx.send('Bilen sover')
#            else:
#                try:
#                    climate = v.climate.get_state()
#                    await ctx.send(':thermometer: ' + str(climate['inside_temp'])+'C inne, '+str(climate['outside_temp']) + 'C ute.')
#                except Exception as e:
#                    print(e)


 #   @commands.command()
 #   async def lys(self,ctx):
 #       """Blinker med lysa"""
 #       vehicles = client.list_vehicles()
 #       for v in vehicles:
 #           if(v.state == 'asleep'):
 #               await ctx.send('Bilen sover')
 #           else:
 #               try:
 #                   lys = v.controls.flash_lights()
 #                   await ctx.send(':flashlight:')
 #               except Exception as e:
 #                   print(e)


#    @commands.command()
#    async def odometer(self,ctx):
#        """Viser odometerstatus"""
#        vehicles = client.list_vehicles()
#        for v in vehicles:
#            if(v.state == 'asleep'):
#                await ctx.send('Bilen sover')
#            else:
#                try:
#                    state = v.get_state()
#                    mile = 1.609344
#                    await ctx.send(str(state['odometer']*mile) + ' km')
#                except Exception as e:
#                    print(e)

    @commands.command()
    async def tut(self,ctx):
        """Tuter med hornet"""
        await ctx.send(':loud_sound:')

    @commands.command()
    async def kjor(self,ctx,destinasjon):
        """Kjører til <destinasjon>"""
        c = 
        data = {
                "type": "share_ext_content_raw",
                "value[android.intent.extra.TEXT]": destinasjon,
                "locale": "en-US",
                "timestamp_ms": int(time.time())
                }

        await ctx.send('Kjører til ' + destinasjon + ' :red_car:')


def setup(bot):
    n = Tesla(bot)
    bot.add_cog(n)
    


