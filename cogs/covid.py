from discord.ext import commands
import json
import discord
import os
import random
from collections import defaultdict
import time
from hashlib import md5
import networkx as nx
import matplotlib.pyplot as plt
from os.path import expanduser
datafil = expanduser("~/credentials/covid.json")
colordict={"generelt":"e6d257"}
def channelcolor(channel_id):
    return md5(str(channel_id).encode('utf-8')).hexdigest()[:6]

def quad(x, max_prob=5):
    return max_prob*x**2

def infect_chance(prev_messages):
    if prev_messages:
        return quad(sum(prev_messages)/len(prev_messages))
    return 0

class covid(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
        self.contaminated = defaultdict(list)
        self.source = []

    @commands.Cog.listener()
    async def on_message(self, message):
        if (message.author.bot or message.channel.id==485544059612561490):
            return
        with open(datafil, 'r',encoding='utf8') as f:
            data = json.load(f)
        if str(message.author.id) in data['infected']:
            self.contaminated[message.channel.id].append(1)
            self.source=str(message.author.id)
            if ('host' in message.content):
                await message.add_reaction("☣️")
        else:
            self.contaminated[message.channel.id].append(0)
            if (random.randint(1, 100) < infect_chance(self.contaminated[message.channel.id])):
                new_data={str(message.author.id):{"time":str(time.time()), "source":self.source, "name":str(message.author), "channel":str(message.channel), "color":str(message.author.color)}}
                data["infected"].update(new_data)
                with open(datafil, 'w',encoding='utf8') as f:
                    json.dump(data, f)
        if len(self.contaminated[message.channel.id]) > 20:
            self.contaminated[message.channel.id].pop(0)

    @commands.command()
    async def rate(self,ctx, channel: discord.TextChannel=None):
        if (ctx.channel.id==485544059612561490 ):
            if (channel is None):
                channel = ctx.channel
            text=str(self.contaminated[channel.id]) + "infect chance: " + str(infect_chance(self.contaminated[channel.id])) + "%"
            embed = discord.Embed(description=text, color=0x50bdfe)
            await ctx.send(embed=embed)

    @commands.command()
    async def listinfected(self,ctx):
        """Lister en liste med alle de infiserte folka"""
        if (ctx.channel.id==485544059612561490):
            with open(datafil, 'r',encoding='utf8') as f:
                data = json.load(f)
            liste=[]
            for person in data['infected']:
                liste.append(f"<@{person}>")
            embed = discord.Embed(description=", ".join(liste), color=0x50bdfe)
            await ctx.send(embed=embed)

    @commands.command()
    async def infected(self, ctx):
        """Lister antall infected"""
        with open(datafil, 'r',encoding='utf8') as f:
            data = json.load(f)
        await ctx.send(len(data['infected']))

    @commands.command()
    async def testme(self,ctx):
        with open(datafil, 'r',encoding='utf8') as f:
            data = json.load(f)
        if str(ctx.message.author.id) in data['infected']:
            await ctx.message.add_reaction("☣️")
        else:
            await ctx.message.add_reaction("➖")

    @commands.command()
    async def infectedby(self,ctx):
        """Viser hvem som infiserte deg og når"""
        with open(datafil, 'r',encoding='utf8') as f:
            data = json.load(f)
        if str(ctx.message.author.id) in data['infected']:
            synder=data['infected'][f'{ctx.message.author.id}']['source']
            tid=time.ctime(float(data['infected'][f'{ctx.message.author.id}']['time']))+ " UTC"
            embed = discord.Embed(description=f"You were infected by <@{synder}> at {tid} ", color=0x50bdfe)
            await ctx.send(embed=embed)

    @commands.command()
    async def healthcare(self,ctx):
        embed = discord.Embed(description=f"There is an ongoing pandemic. If you get sick, please get into quarantine. Sick people not quarantined will become ill and unable to speak. Some might even die and be banned.", color=0x50bdfe)
        await ctx.send(embed=embed)

    @commands.command()
    async def graph(self,ctx,*args):
        """Tegner et kart over infeksjoner"""
        if (len(args)==0):
            fontsize=12
            fontsize2=12
        elif (1>len(args)>3):
            return
        elif ((0<int(args[0])<100) and (0<int(args[1])<100)):
            fontsize=int(args[0])
            fontsize2=int(args[1])
        graph=nx.DiGraph()
        list=[]
        labeldict= {}
        edgedict = {}
        color_map=['#50bdfe']
        user_color_map=['#50bdfe']
        with open(datafil, 'r',encoding='utf8') as f:
            data = json.load(f)
        for user in data['infected']:
            unit=(data['infected'][user]['source'],user)
            list.append(unit)
            edgedict[unit]=data['infected'][user]['channel']
            labeldict[user] = data['infected'][user]['name']
            #labeldict[user] = ''
            hash=str(channelcolor(data['infected'][user]['channel']))
            hash = '#' + hash
            color_map.append(hash)
            user_color_map.append(data['infected'][user]['color'])
        graph.add_edges_from(list)
        try:
            if (args[2] == '1'):
                pos = nx.circular_layout(graph)
            elif (args[2] == '2'):
                pos = nx.planar_layout(graph)
            else:
                pos = nx.spring_layout(graph)

            nx.draw(graph, pos, labels=labeldict, with_labels = True, font_size=int(fontsize), edge_color=color_map, node_color=user_color_map, font_color='green')
            nx.draw_networkx_edge_labels(graph, pos, edge_labels=edgedict, font_color='red', font_size=fontsize2)
        except Exception as e:
            await ctx.send(e)
        plt.savefig(expanduser('~/graph.png'), transparent=True, dpi=1000)
        await ctx.send(file=discord.File(expanduser('~/graph.png')))
        plt.clf()

def setup(bot):
    n = covid(bot)
    bot.add_cog(n)
