from discord.ext import commands
from collections import Counter
from os.path import expanduser
import asyncpraw, os, discord, time, codecs, json

with open(expanduser("~/credentials/discordConfig.json"), 'r',encoding='utf8') as f:
    data = json.load(f)
    channels=data["channels"]
    owners=data["owners"]

with open(expanduser("~/credentials/norge_bot.config"), 'r',encoding='utf8') as f:
    data = json.load(f)

reddit = asyncpraw.Reddit(client_id=data["client_id"],
                     client_secret=data["client_secret"],
                     user_agent=data["user_agent"],
                     username=data["username"],
                     password=data["password"])
embedColour = 0x50bdfe
userLimit = 5
pmTrigger = 100

class Modreddit(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def mail(self,ctx,limits,subs):
        unread_counts = reddit.subreddit(subs).modmail.unread_count()
        print(unread_counts)


    @commands.command()
    async def modq(self,ctx):
        """Skriver modqueue"""
        if ctx.channel.id != channels[0]:
            await ctx.send(f"Dette er ikke <#{channels[0]:}> ಠ\_ಠ")
            return
        elif ctx.channel.id == channels[0]:
            i=0
            try:
                subreddit = await reddit.subreddit("mod")
                await ctx.send(subreddit.mod.modqueue(limit=None).length())
                async for item in subreddit.mod.modqueue(limit=None):
                    if(item.subreddit_name_prefixed=="r/norge"):
                        i=i+1
                        if("t1" in str(item.fullname)):
                            body="**COMMENT**: " + item.body
                        elif("t3" in str(item.fullname)):
                            body="**SUBMISSION**: " + item.title
                        await item.author.load()
                        author_age = int((time.time() - item.author.created_utc)/(60*60*24))
                        if(author_age>365):
                            author_age=str(int(author_age/365)) + " years old"
                        else:
                            author_age=str(author_age) + " days old"
                        if(len(body)<128):
                            desc = body
                        else:
                            desc = body[:125] + "[...]"
                        embed = discord.Embed(description=desc, color=embedColour)
                        #embed.set_thumbnail(url="https://emojipedia-us.s3.dualstack.us-west-1.amazonaws.com/thumbs/160/openmoji/292/construction_1f6a7.png")
                        if (item.num_reports>0):
                            reports=""
                            for report in item.user_reports:
                                reports += report[0] + " (" + str(report[1]) + " rapporter)\n"
                        else:
                            reports="Removed by " + str(item.banned_by)
                        embed.add_field(name="Reports", value=reports, inline=True)
                        embed.add_field(name="Links", value="[Permalink](https://old.reddit.com"+item.permalink+")", inline=True)
                        embed.set_author(name="/u/" + str(item.author) +" "+ author_age, url="https://old.reddit.com/u/"+str(item.author))
                        timeStamp = time.ctime(int(item.created_utc))+ " UTC"
                        embed.set_footer(text=timeStamp)
                        await ctx.send(embed=embed)
            except Exception as e:
                await ctx.send(e)

            if (i == 0):
                await ctx.send("https://b.thumbs.redditmedia.com/7uBqTsML9Y-sksmfvCrKrHvoUANP6ZCOw9UOVIV6aXQ.png")


def setup(bot):
    n = Modreddit(bot)
    bot.add_cog(n)
