import time
from discord.ext import commands
import json
import random
import codecs
from os.path import expanduser

nyttarsaften = 1640991600
nyttarsaften1800 = 1640970000

def get_data():
    with open(expanduser("~/.config/nyttar/nyttar.json"), 'r',encoding='utf8') as f:
        data = json.load(f)
    return data

def send_data(data):
    with open(expanduser("~/.config/nyttar/nyttar.json"), 'w',encoding='utf8') as f:
        json.dump(data,f)
    return data

class newYear(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def syng(self,ctx,sang):
        """Legger deg til på listen de som har sunge en <sang>"""
        if time.time() > nyttarsaften1800:
            data = get_data()
            for entry in data['sunge']:
                if ctx.message.author.id == entry["id"]:
                    await ctx.send("Du har jo alt sunge!")
                    return 0
            entry={"id":ctx.message.author.id,"sang":sang}
            data["sunge"].append(entry)
            send_data(data)
            i = random.randint(1, 100)
            if i < 20:
                await ctx.send("Det kostymet var helt OK.")
            elif i > 20:
                await ctx.send("For et fint kostyme!")
        else:
            await ctx.send("Kan ikke gå nyttårsbukk før nyttårsaften kl 18:00!")

    @commands.command()
    async def skyt(self,ctx):
        """Skyter nyttårsraketter!"""
        if time.time() > nyttarsaften1800:
            i = random.randint(1, 100)
            if i < 10:
                data = get_data()
                print(len(data["sunge"]))
                y = random.randint(0, len(data["sunge"])-1)
                await ctx.send(f'Å nei, du traff <@{data["sunge"][y]["id"]}> med raketten! 🎇')
                if i < 4:
                    await ctx.send("Hen ble blind")
            else:
                await ctx.send(f'🎆')
        else:
            await ctx.send("Det er ulovlig å skyte raketter før 1800! Skal eg melde deg til Politiet?")

    @commands.command()
    async def giGodis(self,ctx,godis):
        """Legger <godis> i godteskålen"""
        message = str(godis) + " fra " + str(ctx.message.author)
        data = get_data()
        for entry in data["skal"]:
            if str(entry["id"]) == str(ctx.message.author.id):
                if str(entry["godis"]) == str(godis):
                    await ctx.send("Du har alt lagt i en sånn en")
                    return 0
        if "@" in str(message):
            await ctx.send("<:CarlsenPalm:330483537298063360>")
            return 0
        entry={"id":str(ctx.message.author.id),"name":str(ctx.message.author),"godis":str(godis),"tatt":"False"}
        data['skal'].append(entry)
        send_data(data)
        await ctx.send("La " + message + " i skåla!")

    @commands.command()
    async def taGodis(self,ctx):
        """De som har sunge kan ta godis"""
        data = get_data()
        for entry in data['harTatt']:
            if str(ctx.message.author.id) == str(entry['id']):
                await ctx.send("Det er bare èn per person! Du har jo alt tatt " + entry['godis'] + " fra " + entry['fra'] + "!")
                return 0
        for entry in data['sunge']:
            if str(ctx.message.author.id) == str(entry['id']):
                i=random.randint(0,len(data['skal'])-1)
                while(data['skal'][i]['tatt']=="True"):
                    i=random.randint(0,len(data['skal'])-1)
                godis=data["skal"][i]
                message = "Du tok " + godis["godis"] + " fra " + godis["name"] + "!"
                entry={"id":ctx.message.author.id,"godis":godis["godis"],"fra":godis["name"]}
                data["harTatt"].append(entry)
                data["skal"][i]["tatt"]="True"
                send_data(data)
                await ctx.send(message)
                return 0
        await ctx.send("Du har jo ikke sunge enda!")

    @commands.command()
    async def seEtterGodis(self,ctx):
        """Ser etter goids i godteskåla"""
        data = get_data()
        i=random.randint(0,len(data['skal'])-1)
        while(data['skal'][i]['tatt']=="True"):
            i=random.randint(0,len(data['skal'])-1)
        await ctx.send("Du ser " + data['skal'][i]['godis'] + " fra " + data['skal'][i]['name'] + "! Det har vært " + str(len(data['skal'])) + " godiser i skåla!")

def setup(bot):
    n = newYear(bot)
    bot.add_cog(n)
