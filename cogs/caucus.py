from discord.ext import commands
from bs4 import BeautifulSoup
import requests

class caucus(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def result(self, ctx, candidate, state):
        """Prints results of the <candidate> from <state>"""
        try:
            url=f"https://www.politico.com/2020-election/results/{state}/"
            html_content = requests.get(url).text
            soup = BeautifulSoup(html_content, "lxml")
            t=soup.find("div", attrs={"class": "jsx-2271921133 leaderboard-graphic dem-ballot"})
            tr=t.find_all("tr")
            for object in tr[1:-2]:
                name=object.find("div", attrs={"class": "jsx-2271921133 candidate-name"})
                name=str(name)[43:]
                name=name[name.find('<br/>')+5:-6]
                if (str(name).lower() == str(candidate).lower()):
                    result=str(object.find("h3", attrs={"class": "jsx-2271921133"}))
                    lim=result.find("<!--")
                    result=result[27:lim]
                    votes=str(object.find_all(("td"), attrs={"class": "jsx-2271921133 lighter number popular-vote desktop-only"}))
                    votes=votes[votes.find('">')+2:votes.find("</td>")]
                    await ctx.send(f"{name} har {votes} stemmer, {result}%")
        except Exception as e:
            await ctx.send("Rusk i maskineriet")

    @commands.command()
    async def counted(self,ctx,state):
        """Shows number of precincts reporting in <state>"""
        try:
            url=f"https://www.politico.com/2020-election/results/{state}/"
            html_content = requests.get(url).text
            soup = BeautifulSoup(html_content, "lxml")
            t=soup.find("div", attrs={"class": "jsx-2271921133 leaderboard-graphic dem-ballot"})
            counted=str(t.find_all("div", attrs={"class": "jsx-2271921133 pull-left"}))
            counted=counted[counted.find('33">')+4:counted.find('</h6>')]
            await ctx.send(counted[:counted.find('<!--')] + "% of precincts reporting")
        except Exceptions as e:
            await ctx.send("Rusk i maskineriet")

def setup(bot):
    n = caucus(bot)
    bot.add_cog(n)
