from discord.ext import commands
import discord
from phue import Bridge
import json
import codecs
import random
import time
import functions
from os.path import expanduser


with open(expanduser("~/credentials/hue.json"), 'r',encoding='utf8') as f:
    data=json.load(f)
    bridgeIP = data["ip"]
    lysliste = data["lysliste"]

b = Bridge(bridgeIP)
b.connect()

class hue(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
    @commands.command()
    async def rave(self,ctx):
        """Kjører 10 sekunder med rave party hos Marlin"""
        functions.log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"command")
        lightlist = []
        for light in b.get_light():
            if b.get_light(int(light), "on") and (b.get_light(int(light),"type")=='Extended color light'):
                lightlist.append(int(light))
        i = 0
        try:
            await ctx.send("Raver")
        except Exception as e:
            print(e)
        while (i < 100):
            b.set_light(lightlist,{'transitiontime':1,'xy':[random.uniform(0,1),random.uniform(0,1)]})
            i = i + 1
        
    @commands.command()
    async def farge(self,ctx,lys,red,green):
        """Setter <lys> til fragle = (<red>, <green>)"""
        functions.log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"command")
        lysliste=[]
        for light in b.get_light():
            lysliste.append(int(light))
        if not b.get_light(int(lys), "on"):
            await ctx.send("Lyset er av")
        elif int(lys) == 9:
            await ctx.send("Nei ikke det lyset")
        elif (float(red) > 1) or (float(green) > 1) or (float(red) < 0) or (float(green) < 0):
            await ctx.send('Fraglene må være mellom 0 og 1. (0,0) er blå')
        elif int(lys) not in lysliste:
            await ctx.send("Finnes bare lysene " + str(lysliste))
        else:
            b.set_light([int(lys)],'xy',[float(red),float(green)])
            await ctx.send("Satt lys " + lys + " til rød = " + red + ", grønn = " + green)
        #if (float(red) > 1) or (float(green) > 1) or (float(red) < 0) or (float(green) < 0):
        #    await ctx.send('Fraglene må være mellom 0 og 1. (0,0) er blå')
        #elif int(lys) not in int(b.get_light()):
        #    await ctx.send("Finnes bare lysene " + str(b.get_light()))
        #elif int(lys) == 9:
        #    await ctx.send("Nei. Ikke det lyset")
        #else:
        #    try:
        #        b.set_light([int(lys)],'xy',[float(red),float(green)])
        #    except Exception as e:
        #        print(e)
        #    await ctx.send("Satt lys " + lys + " til rød = " + red + ", grønn = " + green)

    @commands.command()
    async def random(self,ctx):
        """Setter random lys til random fragle"""
        functions.log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"command")
        lys = random.randint(9,15)
        red = random.uniform(0,1)
        green = random.uniform(0,1)
        b.set_light([lys],'xy',[red,green])
        await ctx.send("Setter lys " + str(lys) + " til rød = " + str(red) + " grønn = " + str(green))

    @commands.command()
    async def clap(self,ctx):
        """Skrur av/på lysene hos Marlin!"""
        functions.log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"command")
        onlist=[]
        offlist=[]
        for light in b.get_light():
            name = b.get_light(int(light), "name")
            if b.get_light(int(light), "on"):
                onlist.append(name)
                b.set_light(int(light), "on",False)
            else:
                offlist.append(name)
                b.set_light(int(light), "on",True)
        #await ctx.send(":clap: Skrur på lysene hos Marlin!")
        await ctx.send(f":clap: Skrur av {onlist} og på {offlist} hos Marlin!")
        
    @commands.command()
    async def listlys(self,ctx):
        """Lister alle lysa"""
        functions.log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"command")
        lightlist = []
        namelist =  []
        for light in b.get_light():
            lightlist.append(light)
            namelist.append(b.get_light(int(light),"name"))
        
        await ctx.send(f"Lysa er {lightlist} med navn {namelist}")
        
        

def setup(bot):
    n = hue(bot)
    bot.add_cog(n)
