from discord.ext import commands
import json
import discord
import codecs
import operator
from collections import Counter
from os.path import expanduser

finaleList={
1:{"artist":"Atle Pettersen", "song":"World on Fire","country":1},
2:{"artist":"Raylee", "song":"Hero","country":2},
3:{"artist":"Stavangerkameratene", "song":"Who I Am","country":3},
4:{"artist":"KIIM", "song":"My Lonely Voice","country":4},
5:{"artist":"Blåsemafian feat. Hazel", "song":"Let Loose","country":5},
6:{"artist":"Emmy", "song":"Witch Woods","country":6},
7:{"artist":"TIX", "song":"Fallen Angel","country":7},
8:{"artist":"Kaja Rode", "song":"Feel Again","country":8},
9:{"artist":"Rein Alexander", "song":"Eyes Wide Open","country":9},
10:{"artist":"IMERIKA", "song":"I Can't Escape","country":10},
11:{"artist":"KEiiNO", "song":"Monument","country":11},
12:{"artist":"Jorn", "song":"Faith Bloody FaITh","country":12}
    }
finaleNumbers = {"🇲🇹":1,"🇦🇱":2,"🇨🇿":3,"🇩🇪":4,"🇷🇺":5,"🇩🇰":6,"🇸🇲":7,"🇲🇰":8,"🇸🇪":9,"🇸🇮":10,"🇨🇾":11,"🇳🇱":12,"🇬🇷":13,"🇮🇱":14,"🇳🇴":15,"🇬🇧":16,"🇮🇸":17,"🇪🇪":18,"🇧🇾":19,"🇦🇿":20,"🇫🇷":21,"🇮🇹":22,"🇷🇸":23,"🇨🇭":24,"🇦🇺":25,"🇪🇸":26} 
esc = ['🇦🇱', '🇦🇲', '🇦🇺', '🇦🇹', '🇦🇿', '🇧🇾', '🇧🇪', '🇧🇦', '🇧🇬', '🇭🇷', '🇨🇾', '🇨🇿', '🇩🇪', '🇪🇪', '🇫🇮', '🇫🇷', '🇬🇪', '🇩🇪', '🇬🇷', '🇭🇺', '🇮🇸', '🇮🇪', '🇮🇱', '🇮🇹', '🇱🇻', '🇱🇹', '🇲🇹', '🇲🇩', '🇲🇪', '🇳🇱', '🇲🇰', '🇳🇴', '🇵🇱', '🇵🇹', '🇷🇴', '🇷🇺', '🇸🇲', '🇷🇸', '🇸🇰', '🇸🇮', '🇪🇸', '🇸🇪', '🇨🇭', '🇹🇷', '🇺🇦', '🇬🇧', '🇩🇰']
embedColour = 0x50bdfe
invert = lambda d: d.__class__(map(reversed, d.items()))
votelist = [12,10,8,7,6,5,4,3,2,1,0]

def log(time,user,message,channel,file):
    f= open(expanduser("~/credentials/") + file + ".log","a+")
    f.write(str(time) + " " + str(user) + " " + str(message) + " " + str(channel)+"\n")
    f.close()
    print(time,user,message,channel)
    
def standings():
    stillingen = {}
    with codecs.open(expanduser("~/credentials/mgp.json"), 'r',encoding='utf8') as f:
        data = json.load(f)
    count = Counter({})
    for (_, voter) in data.items():
        inverted = invert(voter)
        inverted = {k: int(v) for k, v in inverted.items()}
        count += Counter(inverted)
    return(count)
    
    
def votes(points,country,author,time):
    with codecs.open(expanduser("~/credentials/mgp.json"), 'r',encoding='utf8') as f:
            data = json.load(f)
    if points in data[str(author)]:
        itsVotes={}
        for index in list(data[str(author)]):
            if data[str(author)][index]==country or int(index)<1:
                del data[str(author)][index]
            elif int(index) > int(points):
                itsVotes[int(index)] = data[str(author)][index]
            elif int(index) in [12,10]:
                itsVotes[int(index)-2] = data[str(author)][index]
            elif (str(int(index)+1) in data[str(author)].keys()) or (int(index) == int(points)) or (str((int(index)+2) in data[str(author)].keys()) and (int(index)==8)):
                itsVotes[int(index)-1] = data[str(author)][index]
            else:
                itsVotes[int(index)] = data[str(author)][index]
        itsVotes[points] = country
        with codecs.open(expanduser("~/credentials/mgp.json"), 'w',encoding='utf8') as f:
            sorted_keys = sorted(itsVotes.keys(),key=lambda key: int(key), reverse=True)
            sorted_dict = {}
            for i in sorted_keys:
                sorted_dict[i]=itsVotes[i]
            data[str(author)]=sorted_dict
            json.dump(data,f)
    elif country in data[str(author)].values():
        indexPoeng = invert(data[str(author)])[country]
        with open(expanduser("~/credentials/mgp.json"), 'w',encoding='utf8') as f:
            data[str(author)].pop(indexPoeng)
            data[str(author)].update({points:country})
            sorted_keys = sorted(data[str(author)].keys(),key=lambda key: int(key), reverse=True)
            sorted_dict = {}
            for i in sorted_keys:
                sorted_dict[i]=data[str(author)][i]
            data[str(author)]=sorted_dict
            json.dump(data,f)
    else:
        if str(author) in data:
            with open(expanduser("~/credentials/mgp.json"), 'w',encoding='utf8') as f:
                data[str(author)].update({points:country})
                sorted_keys = sorted(data[str(author)].keys(),key=lambda key: int(key), reverse=True)
                sorted_dict = {}
                for i in sorted_keys:
                    sorted_dict[i]=data[str(author)][i]
                data[str(author)]=sorted_dict
                json.dump(data,f)
    file="mgpfinale"
    f= open(expanduser("~/credentials/") + file + ".log","a+")
    f.write(str(time) + " " + str(standings()) + "\n")
    f.close()
    return(points + " points to " + country)
    
class mgp(commands.Cog):
    def __init__(self, bot):
        self.bot = bot    
        
#    @commands.command()
#    async def eurohelp(self,ctx):
#        """Prints helpfull message"""
#        await ctx.send("This bot keeps track of discord votes on the Eurovision Song Contest. To be able to vote, you have to ``€enter``. You can only vote on 10 countries. You can only give 12, 10, 8, 7, 6, 5, 4, 3, 2, or 1 as points. You can change your votes at any time. Giving the same amount of points to the a new country, results in the newest vote pushing the other votes down the vote list. For syntaxt, ``€help eurovision``")
#    
    @commands.command()
    async def songlist(self,ctx): 
        """Lists all the songs of the Finale, in order"""
        desc = ""
        for i in [1,2,3,4,5,6,7,8]:
            desc = desc + "\n ***" + str(i) + "*** " + finaleList[i]["song"] + " sunget av " + finaleList[i]["artist"] 
        embed = discord.Embed(description=desc, title="MGP Finale", color=embedColour)
        desc = ""
        for i in [9,10,11,12]: #,13,14,15,16]:
            desc = desc + "\n ***" + str(i) + "*** " + finaleList[i]["song"] + " sunget av " + finaleList[i]["artist"] 
            #desc = desc + "\n ***" + str(i) + "*** " + finaleList[i]["country"] + " " + finaleList[i]["song"] + " sung by " + finaleList[i]["artist"] + " in " + finaleList[i]["lang"] 
        embed2 = discord.Embed(description=desc, color=embedColour)
        #desc = ""
        #for i in [17,18,19,20,21,22,23,24,25,26]:
        #    desc = desc + "\n ***" + str(i) + "*** " + finaleList[i]["country"] + " " + finaleList[i]["song"] + " sung by " + finaleList[i]["artist"] + " in " + finaleList[i]["lang"] 
        #embed3 = discord.Embed(description=desc, color=embedColour)
        await ctx.send(embed=embed)
        await ctx.send(embed=embed2)
        #await ctx.send(embed=embed3)
    
    @commands.command()
    async def stem(self,ctx,points,country: commands.clean_content):
        """Stem <points> på <artistnumber>"""
        log(ctx.message.created_at,ctx.message.author,ctx.message.content,ctx.message.channel,"mgpvote")
        with open(expanduser("~/credentials/mgp.json"), 'r',encoding='utf8') as f:
            data = json.load(f)
        if str(ctx.message.author.id) not in data:
            await ctx.send("You have not entered the voting. Please ``€enter`` first.")
        elif int(points) not in votelist:
            await ctx.send("You can only give 12, 10, 8, 7, 6, 5, 4, 3, 2, or 1 as points.")
 #       elif country not in finale:
 #           try:
 #               if 0 <= int(country) <= 10: #26:
 #                   await ctx.send(votes(points,finaleList[int(country)]['country'],ctx.message.author.id,ctx.message.created_at))
#                else:
 #                   await ctx.send("This country is not in the Finale. You can only vote for " + str(finaleList))
#           except Exception as e:
#              if isinstance(country,str):
#                    await ctx.send("This country is not in the Finale. You can only vote for " + str(finale))
#                else:
#                    await ctx.send("Voting error, contact <@202745416062599168>")
 #               
        else:
            await ctx.send(votes(points,country,ctx.message.author.id,ctx.message.created_at))
        
        
 #   @commands.command()
 #   async def myVotes(self,ctx):
 #       """Lists your votes"""
 #       with open(expanduser("~/credentials/eurovision.json"), 'r',encoding='utf8') as f:
 #           data = json.load(f)
 #       if str(ctx.message.author.id) in data:
 #           s = ""
 #           for index in data[str(ctx.message.author.id)].keys():
 #               s = s  + data[str(ctx.message.author.id)][index] + index + "   "
 #           desc = s
 #           embed = discord.Embed(description=desc, color=embedColour)
 #           await ctx.send(embed=embed)
 #       else:
 #           await ctx.send("You have not entered the voting. Please ``€enter`` first.")
        
    @commands.command()
    async def resultat(self,ctx):
        """Current leaderboard"""
        count=standings()
        desc = ('\n'.join([f"{i} with {n} points" for i, n in count.most_common()]))
        embed = discord.Embed(description=desc, title="MGP Finale", color=embedColour)
        await ctx.send(embed=embed)

 
    @commands.command()
    async def enter(self,ctx):
        """Enters you to the voting able list"""
        with open(expanduser("~/credentials/mgp.json"), 'r',encoding='utf8') as f:
            data = json.load(f)
        if str(ctx.message.author.id) in data:
            await ctx.send("You have already entered.")
        else:
            with open(expanduser("~/credentials/mgp.json"), 'w',encoding='utf8') as f:
                data.update({str(ctx.message.author.id):{}})
                await ctx.send("We have recieved your entry.")
                json.dump(data,f)
                
#    @commands.command()
#    async def song(self,ctx,ctrysongnum):
#        """Shows information about the song"""
#        if ctrysongnum in finale:
#            embed = discord.Embed( title="Eurovision Finale", color=embedColour)
#            land = finaleList[finaleNumbers[ctrysongnum]]["country"]
#            artist = finaleList[finaleNumbers[ctrysongnum]]["artist"]
#            song = finaleList[finaleNumbers[ctrysongnum]]["song"]
#            lang = finaleList[finaleNumbers[ctrysongnum]]["lang"]
#            txt = f"**{finaleNumbers[ctrysongnum]}** {land} "
#            txt_sang = f"{song} sung by {artist} in {lang}"
#            embed.add_field(name=txt, value=txt_sang, inline=False)
#            await ctx.send(embed=embed)
#        elif 0 < int(ctrysongnum) <= 26:
#            embed = discord.Embed( title="Eurovision Finale", color=embedColour)
#            land = finaleList[int(ctrysongnum)]["country"]
#            artist = finaleList[int(ctrysongnum)]["artist"]
#            song = finaleList[int(ctrysongnum)]["song"]
#            lang = finaleList[int(ctrysongnum)]["lang"]
#            txt = f"**{ctrysongnum}** {land} "
#            txt_sang = f"{song} sung by {artist} in {lang}"
#            embed.add_field(name=txt, value=txt_sang, inline=False)
#            await ctx.send(embed=embed)
#        else:
#            await ctx.send("This country is not in the Finale. Try " + str(finale) + " instead.")
    
    
    @commands.command()
    async def stream(self,ctx):
        """Norwegian Stream"""
        await ctx.send("https://tv.nrk.no/direkte/nrk1")
    
    
def setup(bot):
    n = mgp(bot)
    bot.add_cog(n)
    
