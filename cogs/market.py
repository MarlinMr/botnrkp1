from discord.ext import commands
import os
import discord
import json
import codecs
import time
import functions
from os.path import expanduser
embedColour = 0x50bdfe

class market(commands.Cog):
    def __init__(self, bot):
        self.bot = bot
    
    @commands.command()
    async def noter(self,ctx):
        """Børsnoterer deg"""
        user = str(ctx.message.author.id)
        with open(expanduser("~/credentials/market.json"), 'r',encoding='utf8') as f:
            data = json.load(f)
        if (user in data["stocks"]):
            await ctx.send("Du er alt børsnotert")
            return 0
        else:
            try:
                data["stocks"].append(user)
                data["bank"][user]=10000  
                data["holdings"][user]={user:10000}
                data["bids"][user]={}
                data["asks"][user]={}
                with open(expanduser("~/credentials/market.json"), 'w',encoding='utf8') as f:
                    json.dump(data,f)
                await ctx.message.add_reaction("✔️")
            except Exception as e:
                print(e)

    @commands.command()
    async def holdings(self,ctx):
        """Viser din portefjøle"""
        user = str(ctx.message.author.id)
        with open(expanduser("~/credentials/market.json"), 'r',encoding='utf8') as f:
            data = json.load(f)
        if (user in data["stocks"]):
            porto = data["holdings"][user]
            desc = ""
            for stock in porto:
                n = data['holdings'][user][stock]
                desc += f"{n} <@{stock}>"
            cash = data["bank"][user]
            desc = str(cash) + ' kr\n' + desc
            embed = discord.Embed(description=desc, title="Portefjøle", color=embedColour)
            await ctx.send(embed=embed)
        else:    
            await ctx.send("Du er ikke børsnotert")
            return 0

    @commands.command()
    async def buy(self,ctx,qty,ticker,bid):
        """Kjøper <antall> <aksje> for <pris>"""
        ticker = ticker[3:-1]
        qty=int(qty)
        bid=int(bid)
        #if functions.is_admin(ctx.message.author.id):
        if True:
            user = str(ctx.message.author.id)
            with open(expanduser("~/credentials/market.json"), 'r',encoding='utf8') as f:
                data = json.load(f)
            # Sjekk at brukeren er notert    
            if (user in data["stocks"]):
                # Sjekk at brukeren har nok penger
                if (int(qty)*float(bid)<=float(data["bank"][user])):
                    # Sjekk at aksjen finnes, unødvendig men too late
                    if (ticker in data["stocks"]):
                        # Sjekk om noen selger fra før ")
                        for seller in data["asks"][ticker]:
                            for price in data["asks"][ticker][seller]:
                                # Sjekk om noen ber om riktig pris")
                                if (int(price) <= bid):
                                    # Sjekk om det er flere asks enn det som kjøpes")
                                    if (qty <= data["asks"][ticker][seller][price]):
                                        data["asks"][ticker][seller][price]=qty-int(data["asks"][ticker][seller][price])
                                        data["holdings"][seller][ticker]=qty-int(data["holdings"][seller][ticker])
                                        data["bank"][seller]=float(data["bank"][seller])+qty*float(price)
                                        data["bank"][user]=float(data["bank"][user])-qty*float(price)
                                        if (ticker in data["holdings"][user]):
                                            data["holdings"][user][ticker]=qty+data["holdings"][user][ticker]
                                        else:
                                            data["holdings"][user][ticker]=qty
                                        qty = 0
                                        now=time.time()
                                        data["trades"][user]={now:{ticker:{price:qty}}}
                                        data["trades"][seller]={now:{ticker:{price:-qty}}}
                                        with open(expanduser("~/credentials/market.json"), 'w',encoding='utf8') as f:
                                            json.dump(data,f)
                                        return await ctx.send(f"Du kjøpte {qty} <@{ticker}> fra <@{seller} for {qty*price} kr")
                                    else:
                                        data["holdings"][seller][ticker]-=data["asks"][ticker][seller][price]
                                        data["asks"][ticker][seller][price]=0
                                        data["bank"][seller]=float(data["bank"][seller]) + data["asks"][ticker][seller][price]*price
                                        data["bank"][user]=float(data["bank"][user])-data["asks"][ticker][seller][price]*price
                                        data["holdings"][user][ticker]=int(data["holdings"][user][ticker])+data["asks"][ticker][seller][price]
                                        now=time.time()
                                        data["trades"][user]={now:{ticker:{price:data["asks"][ticker][seller][price]}}}
                                        data["trades"][seller]={now:{ticker:{price:-data["asks"][ticker][seller][price]}}}
                                        await ctx.send(f"Du solgte {data['asks'][ticker][seller][price]} <@{ticker}> til <@{seller} for {data['asks'][ticker][seller][price]*price}")
                                        qty -= data["asks"][ticker][seller][price]
                        # Sjekker om brukeren selger fra før
                        if (user in data["bids"][ticker]):
                            # Sjekker om brukeren selger for samme prisen fra før
                            if (bid in data["bids"][ticker][user]):
                                data["bids"][ticker][user][bid]+=int(qty)
                            else:
                                data["bids"][ticker][user][bid]=int(qty)
                        else:
                            data["bids"][ticker][user]={int(bid):int(qty)}
                        with open(expanduser("~/credentials/market.json"), 'w',encoding='utf8') as f:
                            json.dump(data,f)
                        await ctx.send(f"Bud for {qty} <@{ticker}> @ {bid} kr motatt")
                    else:
                        await ctx.send(f"{ticker} aksjen er ikke børsnotert")
                else:
                    await ctx.send("Ikke nok dekning")
            else:    
                await ctx.send("Du er ikke børsnotert")
        #else:
        #    await ctx.send("Børsen er ikke åpen enda")

    @commands.command()
    async def sell(self,ctx,qty,ticker,ask):
        """Selger <antall> <aksje> for <pris>"""
        ticker = ticker[3:-1]
        qty=int(qty)
        ask=int(ask)
        if (functions.is_admin(ctx.message.author.id)):
            user = str(ctx.message.author.id)
            with open(expanduser("~/credentials/market.json"), 'r',encoding='utf8') as f:
                data = json.load(f)
            # Sjekk at brukeren er notert
            if (user in data["stocks"]):
                # Sjekk at brukeren eier aksjen
                if (int(qty)<=float(data["holdings"][user][ticker])):
                    # Sjekk at aksjen finnes, unødvendig men too late
                    if (ticker in data["stocks"]):
                        # Sjekk om noen byr fra før 
                        for buyer in data["bids"][ticker]:
                            await ctx.send("Sjekker buyer for bud")
                            for price in data["bids"][ticker][buyer]:
                                # Sjekk om noen byr riktig pris
                                if (int(price) >= ask):
                                    # Sjekk om det er flere bud enn det som selges
                                    if (qty <= data["bids"][ticker][buyer][price]):
                                        data["bids"][ticker][buyer][price]-=qty
                                        data["holdings"][buyer][ticker]+=qty
                                        data["bank"][buyer]-=qty*ask
                                        data["bank"][user]+=qty*ask
                                        data["holdings"][user][ticker]-=qty
                                        qty = 0
                                        now=time.time()
                                        data["trades"][user]={now:{ticker:{ask:qty}}}
                                        data["trades"][buyer]={now:{ticker:{ask:-qty}}}
                                        return await ctx.send(f"Du solgte {qty} <@{ticker}> til <@{buyer} for {qty*ask} kr")
                                    else:
                                        data["holdings"][buyer][ticker]+=data["bids"][ticker][buyer][price]
                                        data["bids"][ticker][buyer][price]=0
                                        data["bank"][buyer]-=data["bids"][ticker][buyer][price]*ask
                                        data["bank"][user]+=data["bids"][ticker][buyer][price]*ask
                                        data["holdings"][user][ticker]-=data["bids"][ticker][buyer][price]
                                        now=time.time()
                                        data["trades"][user]={now:{ticker:{ask:data["bids"][ticker][buyer][price]}}}
                                        data["trades"][buyer]={now:{ticker:{ask:-data["bids"][ticker][buyer][price]}}}
                                        await ctx.send(f"Du solgte {data['bids'][ticker][buyer][price]} <@{ticker}> til <@{buyer} for {data['bids'][ticker][buyer][price]*ask}")
                                        qty -= data["bids"][ticker][buyer][price]
                        # Sjekker om brukeren selger fra før
                        if (user in data["asks"][ticker]):
                            # Sjekker om brukeren selger for samme pris fra før
                            if (ask in data["asks"][ticker][user]):
                                data["asks"][ticker][user][ask]+=int(qty)
                            else:
                                data["asks"][ticker][user][ask]=int(qty)
                        else:
                            data["asks"][ticker][user]={int(ask):int(qty)}
                        with open(expanduser("~/credentials/market.json"), 'w',encoding='utf8') as f:
                            json.dump(data,f)
                        await ctx.send(f"Ask for {qty} <@{ticker}> @ {ask} kr motatt")
                    else:
                        await ctx.send(f"{ticker} aksjen er ikke børsnotert")
                else:
                    await ctx.send("Ikke nok aksjer")
            else:    
                await ctx.send("Du er ikke børsnotert")
        else:
            await ctx.send("Børsen er ikke åpen enda")

    @commands.command()
    async def ordre(self,ctx,ticker):
        """Lister ordre for <ticker>"""
        ticker = ticker[3:-1]
        with open(expanduser("~/credentials/market.json"), 'r',encoding='utf8') as f:
            data = json.load(f)
        await ctx.send(data["bids"][ticker])
        await ctx.send(data["asks"][ticker])

def setup(bot):
    n = market(bot)
    bot.add_cog(n)
