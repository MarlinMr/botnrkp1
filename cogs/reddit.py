from discord.ext import commands
import praw
import os
import discord
import time
from collections import Counter
import codecs
import json
from os.path import expanduser

with open(expanduser("~/credentials/discordConfig.json"), 'r',encoding='utf8') as f:
    data = json.load(f)
    channels=data["channels"]
    owners=data["owners"]

with open(expanduser("~/credentials/redditConfig.json"), 'r',encoding='utf8') as f:
    data = json.load(f)

reddit = praw.Reddit(client_id=data["client_id"],
                     client_secret=data["client_secret"],
                     user_agent=data["user_agent"],
                     username=data["username"],
                     password=data["password"])
embedColour = 0x50bdfe

userLimit = 5
pmTrigger = 100

class Reddit(commands.Cog):
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def mail(self,ctx,limits,subs):
        unread_counts = reddit.subreddit(subs).modmail.unread_count()
        print(unread_counts)

    @commands.command()
    async def top(self,ctx,limits,subs):
        """Lenker til top <limits> posts i <subs>"""
        if not (str(ctx.message.author.id) in str(owners)):
            if int(limits) <= userLimit:
                submissions = reddit.subreddit(subs).top(limit=int(limits))
                await sendReddit(ctx,submissions)
            elif int(limits) >= pmTrigger:
                submissions = reddit.subreddit(subs).top(limit=int(limits))
                await sendPM(ctx,submissions)
            elif int(limits) < pmTrigger:
                submissions = reddit.subreddit(subs).top(limit=int(userLimit))
                await sendReddit(ctx,submissions)
        elif str(ctx.message.author.id) in str(owners):
            submissions = reddit.subreddit(subs).top(limit=int(limits))
            await sendReddit(ctx,submissions)

    @commands.command()
    async def hot(self,ctx,limits,subs):
        """Lenker til hot <limits> posts i <subs>"""
        if not (str(ctx.message.author.id) in str(owners)):
            if int(limits) <= userLimit:
                submissions = reddit.subreddit(subs).hot(limit=int(limits))
                await sendReddit(ctx,submissions)
            elif int(limits) >= pmTrigger:
                submissions = reddit.subreddit(subs).hot(limit=int(limits))
                await sendPM(ctx,submissions)
            elif int(limits) < pmTrigger:
                submissions = reddit.subreddit(subs).hot(limit=int(userLimit))
                await sendReddit(ctx,submissions)
        elif str(ctx.message.author.id) in str(owners):
            submissions = reddit.subreddit(subs).hot(limit=int(limits))
            await sendReddit(ctx,submissions)

    @commands.command()
    async def new(self,ctx,limits,subs):
        """Lenker til new <limits> posts i <subs>"""
        if not (str(ctx.message.author.id) in str(owners)):
            if int(limits) <= userLimit:
                submissions = reddit.subreddit(subs).new(limit=int(limits))
                await sendReddit(ctx,submissions)
            elif int(limits) >= pmTrigger:
                submissions = reddit.subreddit(subs).new(limit=int(limits))
                await sendPM(ctx,submissions)
            elif int(limits) < pmTrigger:
                submissions = reddit.subreddit(subs).new(limit=int(userLimit))
                await sendReddit(ctx,submissions)
        elif str(ctx.message.author.id) in str(owners):
            submissions = reddit.subreddit(subs).new(limit=int(limits))
            await sendReddit(ctx,submissions)

    @commands.command()
    async def q(self,ctx):
        """Skriver modqueue"""
        if ctx.channel.id != channels[0]:
            await ctx.send(f"Dette er ikke <#{channels[0]:}> ಠ\_ಠ")
            return
        elif ctx.channel.id == channels[0]:
            await ctx.send("======= Printar modkø for r/norge =======")
            modqueue = reddit.subreddit('mod').mod.modqueue(limit=None)
            i=0
            for item in modqueue:
                if(item.subreddit_name_prefixed=="r/norge"):
                    i=i+1
                    if str(item.mod_reports) != '[]':
                        desc = "["+str(item.mod_reports[0])+"]"
                        embed = discord.Embed(description=desc+"(https://old.reddit.com" + item.permalink + "?context=1000)", color=embedColour)
                        embed.set_author(name=item.author,url="https://www.reddit.com/u/"+str(item.author))
                        await ctx.send(embed=embed)
                    elif str(item.user_reports) != '[]':
                        desc = "["+str(item.user_reports[0])+"]"
                        embed = discord.Embed(description=desc+"(https://old.reddit.com" + item.permalink + "?context=1000)", color=embedColour)
                        embed.set_author(name=item.author,url="https://www.reddit.com/u/"+str(item.author))
                        await ctx.send(embed=embed)
                    elif item.banned_by == True:
                        await ctx.send("<https://old.reddit.com" + item.permalink + "?context=1000>")
                        await ctx.send(embed=embed)
                        await ctx.send("{}{}".format(
                            str(item.subreddit_name_prefixed).ljust(10,' '),
                            str(i).ljust(5,' ')
                            ))
                    else:
                        await ctx.send("<https://old.reddit.com" + item.permalink + "?context=1000>")
                        await ctx.send("{}{}".format(
                            str(item.author).ljust(21,' '),
                            str(item.banned_by).ljust(len(item.banned_by)+1,' '),
                            str(item.ban_note)
                            ))
            await ctx.send("======= Ferdig å printa kø =======")
            if i > 0:
                await ctx.send("Det er "+ str(i) +" elementer i køa, hvorfor har du ikke fiksent det ??")
            else:
                await ctx.send("https://b.thumbs.redditmedia.com/7uBqTsML9Y-sksmfvCrKrHvoUANP6ZCOw9UOVIV6aXQ.png")

    @commands.command()
    async def subscribers(self,ctx,sub):
        """Lister subscribers i <sub>"""
        subs = reddit.subreddit(sub).subscribers
        await ctx.send(subs)

async def sendReddit(ctx,submissions):
    for submission in submissions:
            embed = discord.Embed(description=str(submission.score)+" upvotes in "+submission.subreddit_name_prefixed,title=str(submission.title)[0:256], color=embedColour, url="https://www.reddit.com"+str(submission.permalink))
            embed.set_author(name=submission.author,url="https://www.reddit.com/u/"+str(submission.author))
            if(submission.thumbnail=="self" or submission.thumbnail=="default"):
                embed.set_thumbnail(url="https://www.redditstatic.com/desktop2x/img/favicon/favicon-96x96.png")
            else:
                embed.set_thumbnail(url=submission.thumbnail)
            await ctx.send(embed=embed)

async def sendPM(ctx,submissions):
    for submission in submissions:
            embed = discord.Embed(description=str(submission.score)+" upvotes in "+submission.subreddit_name_prefixed,title=str(submission.title)[0:256], color=embedColour, url="https://www.reddit.com"+str(submission.permalink))
            embed.set_author(name=submission.author,url="https://www.reddit.com/u/"+str(submission.author))
            if(submission.thumbnail=="self" or submission.thumbnail=="default"):
                embed.set_thumbnail(url="https://www.redditstatic.com/desktop2x/img/favicon/favicon-96x96.png")
            else:
                embed.set_thumbnail(url=submission.thumbnail)
            await ctx.message.author.send(embed=embed)
 

def setup(bot):
    n = Reddit(bot)
    bot.add_cog(n)
