#!/usr/bin/env python3
# -*- coding: utf-8 -*-
import json
import os
import codecs
import discord
import pprint
import sys
import getpass
from discord.ext import commands
from os.path import expanduser
sys.dont_write_bytecode = True

try:
    with open(expanduser('~/credentials/discordConfig.json'), 'r',encoding='utf8') as f:
        data = json.load(f)
        token = data["token"]
        prefix = data["prefix"]
        status = data["playing"]
except Exception as loadingJSON:
    print(loadingJSON)

bot = commands.Bot(command_prefix=prefix, prefix=prefix, case_insensitive=True, intents=discord.Intents.default())
bot.load_extension(f"cogs.admin")

@bot.event
async def on_ready():
    for guild in bot.guilds:
        print(f'Logged in as: {bot.user.name} in {guild.name}. Version: {discord.__version__}')
    await bot.change_presence(status=status)
    print("Loading cogs...")
#    try:
#        for file in os.listdir(expanduser('~/botnrkp1/cogs')):
#            if (file.endswith("nyttar.py") or file.endswith("stonks.py") or file.endswith("invest.py") or file.endswith("admin.py") or file.endswith("jul.py") or file.endswith("valgresultat.py") or file.endswith("misc.py") or file.endswith("teller.py") or file.endswith("modreddit.py")):
#                name = file[:-3]
#                await bot.load_extension(f"cogs.{name}")
#    except Exception as loadingCogs:
#        print(loadingCogs)

print("Starting bot...")
bot.run(token, reconnect=True)
